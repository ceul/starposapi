
import { Request, Response } from 'express';
import { LogDAOGrowPos } from '../repository/growposDB/logDAO';
import { LogEnum } from '../models/growpos/log.enum';
import { ErrorGrowPos } from '../models/growpos/error';
let log =  new LogDAOGrowPos() 
export class LogController {

	/*-------------------------------- app --------------------------------------------------------*/
	public async insertLog(req: Request, res: Response, next) {
		try {
			res.send(await log.insertLog(req.body.type, req.body.description));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			log.insertLog(LogEnum.ERROR, `${LogController.name} -> ${this.insertLog.name}: ${error}`)
		}
	}

	public async getLog(req: Request, res: Response, next) {
		try {
			res.send(await log.getLog());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${LogController.name} -> ${this.getLog.name}: ${error}`)
		}
	}

}

