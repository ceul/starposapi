
import { Request, Response } from 'express';
import { PaymentDAOGrowPos } from '../repository/growposDB/paymentDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let payment = new PaymentDAOGrowPos();
export class PaymentController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async getByCloseCash(req: Request, res: Response, next) {
		try {
			res.send(await payment.getByCloseCash(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting payment by close cash :' + error + `: ${PaymentController.name} -> getByCloseCash`);
		}
    }
    
    public async getTotalPaymentsByCloseCash(req: Request, res: Response, next) {
		try {
			res.send(await payment.getTotalPaymentsByCloseCash(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting total payment by close cash :' + error + `: ${PaymentController.name} -> getTotalPaymentsByCloseCash`);
		}
	}

	public async getTransactionsReport(req: Request, res: Response, next) {
		try {
			res.send(await payment.getTransactionsReport(req.body.datestart, req.body.dateend));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting transactions :' + error + `: ${PaymentController.name} -> getTransactionsReport`);
		}
	}

	public async getTransactionsReportByPayment(req: Request, res: Response, next) {
		try {
			res.send(await payment.getTransactionsReportByPayment(req.body.datestart, req.body.dateend));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting transactions :' + error + `: ${PaymentController.name} -> getTransactionsReport`);
		}
	}
	
}

