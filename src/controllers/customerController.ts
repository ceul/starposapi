
import { Request, Response } from 'express';
import { CustomerDAOGrowPos } from '../repository/growposDB/customerDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let customer = new CustomerDAOGrowPos();
export class CustomerController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertCustomer(req: Request, res: Response, next) {
		try {
			res.send(await customer.insertCustomer(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting customer :' + error + `: ${CustomerDAOGrowPos.name} -> insertCustomer`);
		}
	}

	public async getCustomers(req: Request, res: Response, next) {
		try {
			res.send(await customer.getCustomers());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting customers :' + error + `: ${CustomerDAOGrowPos.name} -> getCustomers`);
		}
	}

	public async getTransactions(req: Request, res: Response, next) {
		try {
			res.send(await customer.getTransactions(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting customers :' + error + `: ${CustomerDAOGrowPos.name} -> getTransactions`);
		}
	}

	public async getCustomerById(req: Request, res: Response, next) {
		try {
			res.send(await customer.getCustomerById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting customer :' + error + `: ${CustomerDAOGrowPos.name} -> getCustomerById`);
		}
	}

	public async getCustomerFiltered(req: Request, res: Response, next) {
		try {
			res.send(await customer.getCustomerFiltered(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting customer filtered :' + error + `: ${CustomerDAOGrowPos.name} -> getCustomerFiltered`);
		}
	}

	public async updateCustomer(req: Request, res: Response, next) {
		try {
			res.send(await customer.updateCustomer(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating customer :' + error + `: ${CustomerDAOGrowPos.name} -> updateCustomer`);
		}
	}

	public async deleteCustomer(req: Request, res: Response, next) {
		try {
			res.send(await customer.deleteCustomer(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting customer :' + error + `: ${CustomerDAOGrowPos.name} -> deleteCustomer`);
		}
	}
}

