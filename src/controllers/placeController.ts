
import { Request, Response } from 'express';
import { PlaceDAOGrowPos } from '../repository/growposDB/placeDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let place = new PlaceDAOGrowPos();
export class PlaceController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertPlace(req: Request, res: Response, next) {
		try {
			res.send(await place.insertPlace(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting place :' + error + `: ${PlaceController.name} -> insertPlace`);
		}
	}

	public async getPlace(req: Request, res: Response, next) {
		try {
			res.send(await place.getPlace(req.body.type));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting place :' + error + `: ${PlaceController.name} -> getPlace`);
		}
	}

	public async getPlaceById(req: Request, res: Response, next) {
		try {
			res.send(await place.getPlaceById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting place :' + error + `: ${PlaceController.name} -> getPlaceById`);
		}
	}

	public async updatePlace(req: Request, res: Response, next) {
		try {
			res.send(await place.updatePlace(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating place :' + error + `: ${PlaceController.name} -> updatePlace`);
		}
	}

	public async deletePlace(req: Request, res: Response, next) {
		try {
			res.send(await place.deletePlace(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting place :' + error + `: ${PlaceController.name} -> deletePlace`);
		}
	}
}

