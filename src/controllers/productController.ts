
import { Request, Response } from 'express';
import { ProductDAOGrowPos } from '../repository/growposDB/productDAO';
import { ErrorGrowPos } from '../models/growpos/error';
import * as sharp from "sharp"
let product = new ProductDAOGrowPos();
export class ProductController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertProduct(req: Request, res: Response, next) {
		try {
			res.send(await product.insertProduct(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting product :' + error + `: ${ProductController.name} -> insertProduct`);
		}
	}

	public async getProducts(req: Request, res: Response, next) {
		try {
			res.send(await product.getProduct());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product :' + error + `: ${ProductController.name} -> getProduct`);
		}
	}

	public async getConstCatProduct(req: Request, res: Response, next) {
		try {
			res.send(await product.getConstCatProduct());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting const category product :' + error + `: ${ProductController.name} -> getProduct`);
		}
	}

	public async getProductByCategory(req: Request, res: Response, next) {
		try {
			res.send(await product.getProductsByCategory(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting products by category :' + error + `: ${ProductController.name} -> getProductByCategory`);
		}
	}

	public async getProductById(req: Request, res: Response, next) {
		try {
			res.send(await product.getProductById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product :' + error + `: ${ProductController.name} -> getProductById`);
		}
	}

	public async getTotalByCloseCash(req: Request, res: Response, next) {
		try {
			res.send(await product.getTotalByCloseCash(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product :' + error + `: ${ProductController.name} -> getTotalTaxesByCloseCash`);
		}
	}

	public async getProductByBarCode(req: Request, res: Response, next) {
		try {
			res.send(await product.getProductByBarCode(req.body.barCode));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product :' + error + `: ${ProductController.name} -> getProductByBarCode`);
		}
	}
	
	public async getProductFiltered(req: Request, res: Response, next) {
		try {
			res.send(await product.getProductFiltered(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product filtered:' + error + `: ${ProductController.name} -> getProductFiltered`);
		}
	}

	public async getProductList(req: Request, res: Response, next) {
		try {
			res.send(await product.getProductList());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product filtered:' + error + `: ${ProductController.name} -> getProductList`);
		}
	}

	public async getProductImage(req: Request, res: Response, next) {
		try {
			res.set('Content_Type', 'image/png')
			let image = await product.getProductImage(req.params.id)
			if(image[0].image === null || image[0].image === undefined){
				throw new Error()
			}
			res.send(image[0].image)
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product image:' + error + `: ${ProductController.name} -> getProductImage`);
		}
	}
	

	public async updateProduct(req: Request, res: Response, next) {
		try {
			res.send(await product.updateProduct(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating product :' + error + `: ${ProductController.name} -> updateProduct`);
		}
	}

	public async uploadPicture(req, res: Response, next) {
		try {
			let buffer = await sharp(req.file.buffer).png().toBuffer()
			res.send(await product.uploadPicture(req.params.id, buffer));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while uploding product picture :' + error + `: ${ProductController.name} -> uploadPicture`);
		}
	}

	public async deleteProduct(req: Request, res: Response, next) {
		try {
			res.send(await product.deleteProduct(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting product :' + error + `: ${ProductController.name} -> deleteProduct`);
		}
	}

	public async getProductBestSelling(req: Request, res: Response, next) {
		try {
			res.send(await product.getProductBestSelling());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while best selling :' + error + `: ${ProductController.name} -> getProductBestSelling`);
		}
	}

	public async getProductSalesReport(req: Request, res: Response, next) {
		try {
			res.send(await product.getProductSalesReport(req.body.datestart, req.body.dateend));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while lower inventory :' + error + `: ${ProductController.name} -> getProductSalesReport`);
		}
	}

	public async getWithLowerInventory(req: Request, res: Response, next) {
		try {
			res.send(await product.getWithLowerInventory());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while lower inventory :' + error + `: ${ProductController.name} -> getWithLowerInventory`);
		}
	}

	public async getProfitSales(req: Request, res: Response, next) {
		try {
			res.send(await product.getProfitSales(req.body.dateStart,req.body.dateEnd));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while lower inventory :' + error + `: ${ProductController.name} -> getProfitSales`);
		}
	}
	
}

