
import { Request, Response } from 'express';
import { OrderDAOGrowPos } from '../repository/growposDB/orderDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let order = new OrderDAOGrowPos();
export class OrderController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertOrder(req: Request, res: Response, next) {
		try {
			res.send(await order.insertOrder(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting order :' + error + `: ${OrderController.name} -> insertOrder`);
		}
	}

	public async getOrder(req: Request, res: Response, next) {
		try {
			res.send(await order.getOrder());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${OrderController.name} -> getOrder`);
		}
	}

	public async getOnProcessOrders(req: Request, res: Response, next) {
		try {
			res.send(await order.getOnProcessOrders());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${OrderController.name} -> getOnProcessOrders`);
		}
	}

	public async getReadyToDeliver(req: Request, res: Response, next) {
		try {
			res.send(await order.getReadyToDeliver());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${OrderController.name} -> getReadyToDeliver`);
		}
	}

	public async getOrderById(req: Request, res: Response, next) {
		try {
			res.send(await order.getOrderById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting order :' + error + `: ${OrderController.name} -> getOrderById`);
		}
	}

	public async updateOrder(req: Request, res: Response, next) {
		try {
			res.send(await order.updateOrder(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating order :' + error + `: ${OrderController.name} -> updateOrder`);
		}
	}

	public async deleteOrder(req: Request, res: Response, next) {
		try {
			res.send(await order.deleteOrder(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting order :' + error + `: ${OrderController.name} -> deleteOrder`);
		}
	}
}

