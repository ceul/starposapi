//    Grow
//    Copyright (c) 2020 GrowAPos
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { ProductCharacteristicDAOGrowPos } from '../repository/growposDB/product_characteristicDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let obj = new ProductCharacteristicDAOGrowPos();
export class ProductCharacteristicController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertProductCharacteristics(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertProductCharacteristic(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting product characteristic :' + error + `: ${ProductCharacteristicController.name} -> insertProductCharacteristics`);
		}
	}

	public async getProductCharacteristicss(req: Request, res: Response, next) {
		try {
			res.send(await obj.getProductCharacteristics());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product characteristic :' + error + `: ${ProductCharacteristicController.name} -> getProductCharacteristicss`);
		}
	}

	public async getProductCharacteristicsById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getProductCharacteristicsById(req.body.id_p, req.body.id_c));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product characteristic :' + error + `: ${ProductCharacteristicController.name} -> getProductCharacteristicsById`);
		}
	}

	public async getProductCharacteristicsByProduct(req: Request, res: Response, next) {
		try {
			res.send(await obj.getProductCharacteristicsByProduct(req.body.id_p));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting product characteristic by product:' + error + `: ${ProductCharacteristicController.name} -> getProductCharacteristicsByProduct`);
		}
	}

	public async updateProductCharacteristics(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateProductCharacteristic(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating characteristic :' + error + `: ${ProductCharacteristicController.name} -> updateProductCharacteristics`);
		}
	}

	public async deleteProductCharacteristics(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteProductCharacteristic(req.body.id_c, req.body.id_p));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting product characteristic :' + error + `: ${ProductCharacteristicController.name} -> deleteProductCharacteristics`);
		}
	}
}