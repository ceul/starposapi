
import { Request, Response } from 'express';
import { LocationDAOGrowPos } from '../repository/growposDB/locationDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let location = new LocationDAOGrowPos();
export class LocationController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertLocation(req: Request, res: Response, next ) {
		try {
			res.send(await location.insertLocation(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting location :' + error + `: ${LocationController.name} -> insertLocation`);
		}
	}

	public async getLocation(req: Request, res: Response, next ) {
		try {
			res.send(await location.getLocation());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${LocationController.name} -> getLocation`);
		}
	}

	public async getLocationById(req: Request, res: Response, next ) {
		try {
			res.send(await location.getLocationById(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting location :' + error + `: ${LocationController.name} -> getLocationById`);
		}
	}

	public async updateLocation(req: Request, res: Response, next ) {
		try {
			res.send(await location.updateLocation(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating location :' + error + `: ${LocationController.name} -> updateLocation`);
		}
	}

	public async deleteLocation(req: Request, res: Response, next ) {
		try {
			res.send(await location.deleteLocation(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting location :' + error + `: ${LocationController.name} -> deleteLocation`);
		}
	}
}

