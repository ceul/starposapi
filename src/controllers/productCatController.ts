
import { Request, Response } from 'express';
import { ProductCatDAOGrowPos } from '../repository/growposDB/productCatDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let productCat = new ProductCatDAOGrowPos();
export class ProductCatController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertProductCat(req: Request, res: Response, next) {
		try {
			res.send(await productCat.insertProductCat(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting productCat :' + error + `: ${ProductCatController.name} -> insertProductCat`);
		}
	}

	public async getProductCat(req: Request, res: Response, next) {
		try {
			res.send(await productCat.getProductCat());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${ProductCatController.name} -> getProductCat`);
		}
	}

	public async getProductCatById(req: Request, res: Response, next) {
		try {
			res.send(await productCat.getProductCatById(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting productCat :' + error + `: ${ProductCatController.name} -> getProductCatById`);
		}
	}

	public async updateProductCat(req: Request, res: Response, next) {
		try {
			res.send(await productCat.updateProductCat(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating productCat :' + error + `: ${ProductCatController.name} -> updateProductCat`);
		}
	}

	public async deleteProductCat(req: Request, res: Response, next) {
		try {
			res.send(await productCat.deleteProductCat(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting productCat :' + error + `: ${ProductCatController.name} -> deleteProductCat`);
		}
	}
}

