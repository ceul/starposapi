//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { MovementsRowDAOGrowAccounting } from '../../repository/growaccountingDB/_movement_rowDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new MovementsRowDAOGrowAccounting();
export class MovementsRowController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertMovementsRow(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertMovementsRow(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting movements row :' + error + `: ${MovementsRowController.name} -> ${this.insertMovementsRow.name}`);
		}
	}

	public async getMovementsRows(req: Request, res: Response, next) {
		try {
			res.send(await obj.getMovementsRows());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting banks :' + error + `: ${MovementsRowController.name} -> ${this.getMovementsRows.name}`);
		}
	}

	public async getMovementsRowsById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getMovementsRowsById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting banks :' + error + `: ${MovementsRowController.name} -> ${this.getMovementsRowsById.name}`);
		}
	}

	public async inactiveMovementsRow(req: Request, res: Response, next) {
		try {
			res.send(await obj.inactiveMovementsRow(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating banks :' + error + `: ${MovementsRowController.name} -> ${this.inactiveMovementsRow.name}`);
		}
	}
}