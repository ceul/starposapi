//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { niifConceptsDAOGrowAccounting } from '../../repository/growaccountingDB/_niif_conceptsDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new niifConceptsDAOGrowAccounting();
export class niifConceptsController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertNiifConcept(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertNiffConcept(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting niif concepts :' + error + `: ${niifConceptsController.name} -> ${this.insertNiifConcept.name}`);
		}
	}

	public async getNiifConcepts(req: Request, res: Response, next) {
		try {
			res.send(await obj.getNiffConcepts());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting niif concepts :' + error + `: ${niifConceptsController.name} -> ${this.getNiifConcepts.name}`);
		}
	}

	public async getNiifConceptsById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getNiffConceptssById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting niif concepts :' + error + `: ${niifConceptsController.name} -> ${this.getNiifConceptsById.name}`);
		}
	}

	public async updateNiifConcept(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateNiffConcept(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating niif concepts :' + error + `: ${niifConceptsController.name} -> ${this.updateNiifConcept.name}`);
		}
	}

	public async deleteNiifConcept(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteNiffConcept(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting niif concepts :' + error + `: ${niifConceptsController.name} -> ${this.deleteNiifConcept.name}`);
		}
	}
}