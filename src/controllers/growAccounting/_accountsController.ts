//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { AccountsDAOGrowAccounting } from '../../repository/growaccountingDB/_accountsDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new AccountsDAOGrowAccounting();
export class AccountsController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertAccount(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertAccount(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting accounts :' + error + `: ${AccountsController.name} -> ${this.insertAccount.name}`);
		}
	}

	public async getAccounts(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAccounts());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting accounts :' + error + `: ${AccountsController.name} -> ${this.getAccounts.name}`);
		}
	}

	public async getAccountsById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAccountsById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting accounts :' + error + `: ${AccountsController.name} -> ${this.getAccountsById.name}`);
		}
	}

	public async updateAccount(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateAccount(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating accounts :' + error + `: ${AccountsController.name} -> ${this.updateAccount.name}`);
		}
	}

	public async deleteAccount(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteAccount(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting accounts :' + error + `: ${AccountsController.name} -> ${this.deleteAccount.name}`);
		}
	}
}