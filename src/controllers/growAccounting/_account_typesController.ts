//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { AccountTypesDAOGrowAccounting } from '../../repository/growaccountingDB/_account_typesDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new AccountTypesDAOGrowAccounting();
export class AccountTypesController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertAccountType(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertAccountType(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting account types :' + error + `: ${AccountTypesController.name} -> insertAccountType`);
		}
	}

	public async getAccountTypes(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAccountTypes());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting account types :' + error + `: ${AccountTypesController.name} -> $getAccountTypes`);
		}
	}

	public async getAccountTypesById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAccountTypesById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting account types :' + error + `: ${AccountTypesController.name} -> getAccountTypesById`);
		}
	}

	public async updateAccountType(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateAccountType(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating account types :' + error + `: ${AccountTypesController.name} -> updateAccountType`);
		}
	}

	public async deleteAccountType(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteAccountType(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting account types :' + error + `: ${AccountTypesController.name} -> ${this.deleteAccountType.name}`);
		}
	}
}