//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { AccountsBanksDAOGrowAccounting } from '../../repository/growaccountingDB/_accounts_banksDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new AccountsBanksDAOGrowAccounting();
export class AccountBanksController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertAccountsBank(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertAccountsBank(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting account banks :' + error + `: ${AccountBanksController.name} -> ${this.insertAccountsBank.name}`);
		}
	}

	public async getAccountsBanks(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAccountsBanks());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting account banks :' + error + `: ${AccountBanksController.name} -> ${this.getAccountsBanks.name}`);
		}
	}

	public async getAccountsBanksById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAccountsBanksById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting account banks :' + error + `: ${AccountBanksController.name} -> ${this.getAccountsBanksById.name}`);
		}
	}

	public async deleteAccountsBank(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteAccountsBank(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting account banks :' + error + `: ${AccountBanksController.name} -> ${this.deleteAccountsBank.name}`);
		}
	}
}