//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { AttributesAccountsDAOGrowAccounting } from '../../repository/growaccountingDB/_attributes_accountsDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new AttributesAccountsDAOGrowAccounting();
export class AttributesAccountsController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertAttributesAccount(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertAttributesAccount(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting attributes accounts :' + error + `: ${AttributesAccountsController.name} -> ${this.insertAttributesAccount.name}`);
		}
	}

	public async getAttributesAccounts(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAttributesAccounts());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting attributes accounts :' + error + `: ${AttributesAccountsController.name} -> ${this.getAttributesAccounts.name}`);
		}
	}

	public async getAttributesAccountsById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAttributesAccountsById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting attributes accounts :' + error + `: ${AttributesAccountsController.name} -> ${this.getAttributesAccountsById.name}`);
		}
	}

	public async deleteAttributesAccount(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteAttributesAccount(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting attributes accounts :' + error + `: ${AttributesAccountsController.name} -> ${this.deleteAttributesAccount.name}`);
		}
	}
}