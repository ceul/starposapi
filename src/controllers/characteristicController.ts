//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { CharacteristicsDAOGrowPos } from '../repository/growposDB/characteristicDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let obj = new CharacteristicsDAOGrowPos();
export class CharacteristicController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertCharacteristics(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertCharacteristic(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting characteristic :' + error + `: ${CharacteristicController.name} -> insertCharacteristics`);
		}
	}

	public async getCharacteristicss(req: Request, res: Response, next) {
		try {
			res.send(await obj.getCharacteristics());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting characteristic :' + error + `: ${CharacteristicController.name} -> getCharacteristicss`);
		}
	}

	public async getCharacteristicsById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getCharacteristicsById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting characteristic :' + error + `: ${CharacteristicController.name} -> getCharacteristicsById`);
		}
	}

	public async updateCharacteristics(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateCharacteristic(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating characteristic :' + error + `: ${CharacteristicController.name} -> updateCharacteristics`);
		}
	}

	public async deleteCharacteristics(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteCharacteristic(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting characteristic :' + error + `: ${CharacteristicController.name} -> deleteCharacteristics`);
		}
	}
}