
import { Request, Response } from 'express';
import { RoleDAOGrowPos } from '../repository/growposDB/roleDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let role = new RoleDAOGrowPos();
export class RoleController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertRole(req: Request, res: Response, next) {
		try {
			res.send(await role.insertRole(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting role :' + error + `: ${RoleController.name} -> insertRole`);
		}
	}

	public async getRole(req: Request, res: Response, next) {
		try {
			res.send(await role.getRole());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${RoleController.name} -> getRole`);
		}
	}

	public async asyncForEach(array, callback) {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index, array);
		}
	}

	public async getRoleById(req: Request, res: Response, next) {
		try {
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting role :' + error + `: ${RoleController.name} -> getRoleById`);
		}
	}

	public async updateRole(req: Request, res: Response, next) {
		try {
			res.send(await role.updateRole(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating role :' + error + `: ${RoleController.name} -> updateRole`);
		}
	}

	public async deleteRole(req: Request, res: Response, next) {
		try {
			res.send(await role.deleteRole(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting role :' + error + `: ${RoleController.name} -> deleteRole`);
		}
	}
}

