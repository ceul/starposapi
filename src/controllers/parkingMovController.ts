//    growPos
//    Copyright (c) 2020 growPos
//    http://growpos.co
//
//    This file is part of growPos
//
//    GrowPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from "express";
import { ParkinMovDAOGrowPos } from "../repository/growposDB/parking_movDAO";
import { Token } from "../models/interfaces/token.interface";
import { ErrorGrowPos } from "../models/growpos/error";
import * as jwt from 'jsonwebtoken'
//import { } from "express-jwt";

let parking_mov = new ParkinMovDAOGrowPos();

export class ParkingMovController {
	/*-------------------------------- app --------------------------------------------------------*/
	public async insertParkingMov(req: Request, res: Response, next) {
		try {
			res.send(await parking_mov.insertParkingMov(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log(
				"An error occurred while inserting parking :" +
				error +
				`: ${ParkingMovController.name} -> insertParkingMov`
			);
		}
	}

	public async getParkingMov(req: Request, res: Response, next) {
		try {
			res.send(await parking_mov.getParkingMov());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log(
				"An error occurred while getting parking :" +
				error +
				`: ${ParkingMovController.name} -> getParkingMov`
			);
		}
	}

	public async getParkingMovById(req: Request, res: Response, next) {
		try {
			res.send(await parking_mov.getParkingMovById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log(
				"An error occurred while getting parking :" +
				error +
				`: ${ParkingMovController.name} -> getParkingMovById`
			);
		}
	}

	public async updateParkingMov(req: Request, res: Response, next) {
		try {
			res.send(await parking_mov.updateParkingMov(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log(
				"An error occurred while updating parking :" +
				error +
				`: ${ParkingMovController.name} -> updateParkingMov`
			);
		}
	}

	public async deleteParkingMov(req: Request, res: Response, next) {
		try {
			res.send(await parking_mov.deleteParkingMov(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log(
				"An error occurred while deleting parking :" +
				error +
				`: ${ParkingMovController.name} -> deleteParkingMov`
			);
		}
	}
}