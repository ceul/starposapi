
import { Request, Response } from 'express';
import { DrawerOpenedDAOGrowPos } from '../repository/growposDB/drawerOpenedDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let drawerOpened = new DrawerOpenedDAOGrowPos();
export class DrawerOpenedController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertDrawerOpened(req: Request, res: Response, next) {
		try {
			res.send(await drawerOpened.insertDrawerOpened(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting drawerOpened :' + error + `: ${DrawerOpenedController.name} -> insertDrawerOpened`);
		}
	}

	public async getDrawerOpened(req: Request, res: Response, next) {
		try {
			res.send(await drawerOpened.getDrawerOpened());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${DrawerOpenedController.name} -> getDrawerOpened`);
		}
	}

	public async getDrawerOpenedByUser(req: Request, res: Response, next) {
		try {
			res.send(await drawerOpened.getDrawerOpenedByUser(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting drawerOpened :' + error + `: ${DrawerOpenedController.name} -> getDrawerOpenedByUser`);
		}
	}

	public async getDrawerOpenedByTicket(req: Request, res: Response, next) {
		try {
			res.send(await drawerOpened.getDrawerOpenedByTicket(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting drawerOpened :' + error + `: ${DrawerOpenedController.name} -> getDrawerOpenedByTicket`);
		}
	}
}

