
import { Request, Response } from 'express';
import { ParkingFeeDAOGrowPos } from '../repository/growposDB/parkingFeeDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let parkingFee = new ParkingFeeDAOGrowPos();
export class ParkingFeeController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertParkingFee(req: Request, res: Response, next) {
		try {
			res.send(await parkingFee.insertParkingFee(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting parkingFee :' + error + `: ${ParkingFeeController.name} -> insertParkingFee`);
		}
	}

	public async getParkingFee(req: Request, res: Response, next) {
		try {
			res.send(await parkingFee.getParkingFee());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting parkingFee :' + error + `: ${ParkingFeeController.name} -> getParkingFee`);
		}
	}

	public async getParkingFeeGroupByType(req: Request, res: Response, next) {
		try {
			res.send(await parkingFee.getParkingFeeGroupByType());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting parkingFee :' + error + `: ${ParkingFeeController.name} -> getParkingFeeGroupByType`);
		}
	}

	public async getParkingFeeById(req: Request, res: Response, next) {
		try {
			res.send(await parkingFee.getParkingFeeById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting parkingFee :' + error + `: ${ParkingFeeController.name} -> getParkingFeeById`);
		}
	}

	public async getParkingFeeByType(req: Request, res: Response, next) {
		try {
			res.send(await parkingFee.getParkingFeeByType(req.body.type));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting parkingFee :' + error + `: ${ParkingFeeController.name} -> getParkingFeeByType`);
		}
	}

	public async updateParkingFee(req: Request, res: Response, next) {
		try {
			res.send(await parkingFee.updateParkingFee(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating parkingFee :' + error + `: ${ParkingFeeController.name} -> updateParkingFee`);
		}
	}

	public async deleteParkingFee(req: Request, res: Response, next) {
		try {
			res.send(await parkingFee.deleteParkingFee(req.body.type));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting parkingFee :' + error + `: ${ParkingFeeController.name} -> deleteParkingFee`);
		}
	}
}

