
import {Request, Response, NextFunction} from "express";
import { AttributeSetController } from "../controllers/attributeSetController";
import * as auth from '../authService'
import * as cors from 'cors'
export class AttributeSetRoutes { 
    
    public attributeSetController: AttributeSetController = new AttributeSetController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/attribute-set')
        .post(auth,this.attributeSetController.insertAttributeSet)

        app.route('/attribute-set/get')
        .post(auth,this.attributeSetController.getAttributeSet)

        app.route('/attribute-set/getById')
        .post(auth,this.attributeSetController.getAttributeSetById)

        app.route('/attribute-set/update')
        .post(auth,this.attributeSetController.updateAttributeSet)

        app.route('/attribute-set/delete')
        .post(auth,this.attributeSetController.deleteAttributeSet)
        
    }
    
}