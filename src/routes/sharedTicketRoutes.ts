
import { Request, Response, NextFunction } from "express";
import { SharedTicketController } from "../controllers/sharedTicketController";
import * as auth from '../authService'
var cors = require('cors');
export class SharedTicketRoutes {

    public sharedTicketController: SharedTicketController = new SharedTicketController();

    public routes(app): void {

        app.use(cors());

        app.route('/shared-ticket')
            .post(auth,this.sharedTicketController.insertSharedTicket)

        app.route('/shared-ticket/get')
            .post(auth,this.sharedTicketController.getSharedTicketList)

        app.route('/shared-ticket/getById')
            .post(auth,this.sharedTicketController.getSharedTicketById)

        app.route('/shared-ticket/update')
            .post(auth,this.sharedTicketController.updateSharedTicket)

        app.route('/shared-ticket/delete')
            .post(auth,this.sharedTicketController.deleteSharedTicket)

    }

}