
import {Request, Response, NextFunction} from "express";
import { ProductController } from "../controllers/productController";
import * as auth from '../authService'
import * as multer from "multer"
var cors = require('cors');
export class ProductRoutes { 
    
    upload = multer({
        limits: {
            fileSize: 1000000
        },
        fileFilter(req, file, cb) {
            if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
                return cb(new Error('File must be a Image'))
            }

            cb(undefined, true)
        }
    })

    public productController: ProductController = new ProductController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/product')
        .post(auth,this.productController.insertProduct);

        app.route('/product/get')
        .post(auth,this.productController.getProducts);

        app.route('/product/getConstCat')
        .post(auth,this.productController.getConstCatProduct);

        app.route('/product/getByCategory')
        .post(auth,this.productController.getProductByCategory);

        app.route('/product/getById')
        .post(auth,this.productController.getProductById);

        app.route('/product/getTotalByCloseCash')
        .post(auth,this.productController.getTotalByCloseCash)

        app.route('/product/getByBarCode')
        .post(auth,this.productController.getProductByBarCode);

        app.route('/product/getFiltered')
        .post(auth,this.productController.getProductFiltered);

        app.route('/product/:id/getProductImage')
        .get(this.productController.getProductImage);

        app.route('/product/getList')
        .post(auth,this.productController.getProductList);

        app.route('/product/update')
        .post(auth,this.productController.updateProduct);

        app.route('/product/:id/uploadPicture')
        .post(auth,this.upload.single('upload'),this.productController.uploadPicture);

        app.route('/product/delete')
        .post(auth,this.productController.deleteProduct);

        app.route('/product/bs')
        .post(auth,this.productController.getProductBestSelling);

        app.route('/product/li')
        .post(auth,this.productController.getWithLowerInventory);

        app.route('/product/getProductSalesReport')
        .post(auth,this.productController.getProductSalesReport);
        
        app.route('/product/getProfitSales')
        .post(auth,this.productController.getProfitSales);
    }
    
}