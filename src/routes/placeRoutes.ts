
import {Request, Response, NextFunction} from "express";
import { PlaceController } from "../controllers/placeController";
import * as auth from '../authService'
var cors = require('cors');
export class PlaceRoutes { 
    
    public placeController: PlaceController = new PlaceController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/place')
        .post(auth,this.placeController.insertPlace)

        app.route('/place/get')
        .post(auth,this.placeController.getPlace)

        app.route('/place/getById')
        .post(auth,this.placeController.getPlaceById)

        app.route('/place/update')
        .post(auth,this.placeController.updatePlace)

        app.route('/place/delete')
        .post(auth,this.placeController.deletePlace)
        
    }
    
}