//    growPos
//    Copyright (c) 2020 growPos
//    http://growpos.co
//
//    This file is part of growPos
//
//    GrowPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPos.  If not, see <http://www.gnu.org/licenses/>.

import {Request, Response, NextFunction} from "express";
import { ParkingMovController } from "../controllers/parkingMovController";
import * as auth from '../authService'
var cors = require('cors');

export class ParkingMovRoutes { 
    
    public parkingMovController: ParkingMovController = new ParkingMovController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/parking_mov')
        .post(auth,this.parkingMovController.insertParkingMov)

        app.route('/parking_mov/get')
        .post(auth,this.parkingMovController.getParkingMov)

        app.route('/parking_mov/getById')
        .post(auth,this.parkingMovController.getParkingMovById)

        app.route('/parking_mov/update')
        .post(auth,this.parkingMovController.updateParkingMov)

        app.route('/parking_mov/delete')
        .post(auth,this.parkingMovController.deleteParkingMov)

    }
    
}