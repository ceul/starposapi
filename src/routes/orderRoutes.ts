
import {Request, Response, NextFunction} from "express";
import { OrderController } from "../controllers/orderController";
import * as auth from '../authService'
var cors = require('cors');
export class OrderRoutes { 
    
    public orderController: OrderController = new OrderController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/order')
        .post(auth,this.orderController.insertOrder)

        app.route('/order/get')
        .post(auth,this.orderController.getOrder)

        app.route('/order/getProcess')
        .post(auth,this.orderController.getOnProcessOrders)

        app.route('/order/getReadyToDeliver')
        .post(auth,this.orderController.getReadyToDeliver)

        app.route('/order/getById')
        .post(auth,this.orderController.getOrderById)

        app.route('/order/update')
        .post(auth,this.orderController.updateOrder)

        app.route('/order/delete')
        .post(auth,this.orderController.deleteOrder)
        
    }
    
}