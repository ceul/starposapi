
import {Request, Response, NextFunction} from "express";
import { CategoryController } from "../controllers/categoryController";
import * as auth from '../authService'
import * as multer from "multer"
var cors = require('cors');
export class CategoryRoutes { 

    upload = multer({
        limits: {
            fileSize: 1000000
        },
        fileFilter(req, file, cb) {
            if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
                return cb(new Error('File must be a Image'))
            }

            cb(undefined, true)
        }
    })
    
    public categoryController: CategoryController = new CategoryController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/category')
        .post(auth,this.categoryController.insertCategory)

        app.route('/category/get')
        .post(auth,this.categoryController.getCategories)

        app.route('/category/getParking')
        .post(auth,this.categoryController.getParkingCategories)

        app.route('/category/getRoot')
        .post(auth,this.categoryController.getRootCategories)

        app.route('/category/getChild')
        .post(auth,this.categoryController.getChildCategories)

        app.route('/category/getById')
        .post(auth,this.categoryController.getCategoryById)

        app.route('/category/update')
        .post(auth,this.categoryController.updateCategory)

        app.route('/category/delete')
        .post(auth,this.categoryController.deleteCategory)
        
        app.route('/category/getBySales')
        .post(auth,this.categoryController.getCategoryBySales)

        app.route('/category/getSalesReport')
        .post(auth,this.categoryController.getCategorySalesReport)

        app.route('/category/:id/getCategoryImage')
        .get(this.categoryController.getCategoryImage);

        app.route('/category/:id/uploadPicture')
        .post(auth,this.upload.single('upload'),this.categoryController.uploadPicture);
        
    }
    
}