//    StarPos
//    Copyright (c) 2020 GrowPos
//    http://starpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import {Request, Response, NextFunction} from "express";
import { CharacteristicController } from "../controllers/characteristicController";
import * as auth from '../authService'
import * as cors from 'cors'
export class CharacteristicsRoutes { 
    
    public objController: CharacteristicController = new CharacteristicController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/characteristic')
        .post(auth,this.objController.insertCharacteristics)

        app.route('/characteristics/get')
        .post(auth,this.objController.getCharacteristicss)

        app.route('/characteristics/getById')
        .post(auth,this.objController.getCharacteristicsById)

        app.route('/characteristic/update')
        .post(auth,this.objController.updateCharacteristics)

        app.route('/characteristic/delete')
        .post(auth,this.objController.deleteCharacteristics)
        
    }
    
}