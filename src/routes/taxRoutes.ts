
import {Request, Response, NextFunction} from "express";
import { TaxController } from "../controllers/taxController";
import * as auth from '../authService'
var cors = require('cors');
export class TaxRoutes { 
    
    public taxController: TaxController = new TaxController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/tax')
        .post(auth,this.taxController.insertTax)

        app.route('/tax/get')
        .post(auth,this.taxController.getTax)

        app.route('/tax/getById')
        .post(auth,this.taxController.getTaxById)

        app.route('/tax/update')
        .post(auth,this.taxController.updateTax)

        app.route('/tax/delete')
        .post(auth,this.taxController.deleteTax)

        app.route('/tax/getByCategory')
        .post(auth,this.taxController.getTaxesByCategory)

        app.route('/tax/getTotalByCloseCash')
        .post(auth,this.taxController.getTotalTaxesByCloseCash)
        
        
    }
    
}