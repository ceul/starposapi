
import {Request, Response, NextFunction} from "express";
import { UnitMeasureController } from "../controllers/unitMeasureController";
import * as auth from '../authService'
var cors = require('cors');
export class UnitMeasureRoutes { 
    
    public unitMeasureController: UnitMeasureController = new UnitMeasureController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/unit-measure')
        .post(auth,this.unitMeasureController.insertUnitMeasure)

        app.route('/unit-measure/get')
        .post(auth,this.unitMeasureController.getUnitMeasure)

        app.route('/unit-measure/getById')
        .post(auth,this.unitMeasureController.getUnitMeasureById)

        app.route('/unit-measure/update')
        .post(auth,this.unitMeasureController.updateUnitMeasure)

        app.route('/unit-measure/delete')
        .post(auth,this.unitMeasureController.deleteUnitMeasure)
        
    }
    
}