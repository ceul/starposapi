
import {Request, Response, NextFunction} from "express";
import { VehicleController } from "../controllers/vehicleController";
import * as auth from '../authService'
var cors = require('cors');
export class VehicleRoutes { 
    
    public vehicleController: VehicleController = new VehicleController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/vehicle')
        .post(auth,this.vehicleController.insertVehicle)

        app.route('/vehicle/get')
        .post(auth,this.vehicleController.getVehicle)

        app.route('/vehicle/getById')
        .post(auth,this.vehicleController.getVehicleById)

        app.route('/vehicle/getByPlate')
        .post(auth,this.vehicleController.getVehicleByPlate)

        app.route('/vehicle/update')
        .post(auth,this.vehicleController.updateVehicle)

        app.route('/vehicle/delete')
        .post(auth,this.vehicleController.deleteVehicle)
        
    }
    
}