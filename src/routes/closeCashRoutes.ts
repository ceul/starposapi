
import {Request, Response, NextFunction} from "express";
import { CloseCashController } from "../controllers/closeCashCotroller";
import * as auth from '../authService'
var cors = require('cors');
export class CloseCashRoutes { 
    
    public closeCashController: CloseCashController = new CloseCashController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/close-cash')
        .post(auth,this.closeCashController.insertCloseCash)

        app.route('/close-cash/get')
        .post(auth,this.closeCashController.getCloseCash)

        app.route('/close-cash/getById')
        .post(auth,this.closeCashController.getCloseCashById)

        app.route('/close-cash/getLastByHost')
        .post(auth,this.closeCashController.getLastCloseCashByHost)

        app.route('/close-cash/getByHostAndBySequence')
        .post(auth,this.closeCashController.getCloseCashByHostAndBySequence)

        app.route('/close-cash/update')
        .post(auth,this.closeCashController.updateCloseCash)

        app.route('/close-cash/isActive')
        .post(auth,this.closeCashController.isCashActive)

        app.route('/close-cash/moveCash')
        .post(auth,this.closeCashController.moveCash)

        app.route('/close-cash/getCloseCashListByDate')
        .post(auth,this.closeCashController.getCloseCashListByDateReport)

        app.route('/close-cash/getCashFlowReport')
        .post(auth,this.closeCashController.getCashFlowReport)
        
    }
    
}