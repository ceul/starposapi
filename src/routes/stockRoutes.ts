
import {Request, Response, NextFunction} from "express";
import { StockController } from "../controllers/stockController";
import * as auth from '../authService'
var cors = require('cors');
export class StockRoutes { 
    
    public stockController: StockController = new StockController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/stock/stockDiary')
        .post(auth,this.stockController.insertStockDiary)

        app.route('/stock/getStock')
        .post(auth,this.stockController.getStock)

        app.route('/stock/getLowStock')
        .post(auth,this.stockController.getLowStock)

        app.route('/stock/getLocationStock')
        .post(auth,this.stockController.getLocationStock)

        app.route('/stock/refactorStockCurrent')
        .post(this.stockController.refactorStockCurrent)

        
    }
}
        