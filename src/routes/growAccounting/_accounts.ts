//    StarPos
//    Copyright (c) 2018 StarPos
//    http://starpos.co
//
//    This file is part of StarPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import {Request, Response, NextFunction} from "express";
import { AccountsController } from "../../controllers/growAccounting/_accountsController";
import * as auth from '../../authService'
import * as cors from 'cors'
export class AccountsRoutes { 
    
    public objController: AccountsController = new AccountsController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/_accounts')
        .post(auth,this.objController.insertAccount)

        app.route('/_accounts/get')
        .post(auth,this.objController.getAccounts)

        app.route('/accounts/getById')
        .post(auth,this.objController.getAccountsById)

        app.route('/_accounts/update')
        .post(auth,this.objController.updateAccount)

        app.route('/_accounts/delete')
        .post(auth,this.objController.deleteAccount)
        
    }
    
}