//    StarPos
//    Copyright (c) 2018 StarPos
//    http://starpos.co
//
//    This file is part of StarPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import {Request, Response, NextFunction} from "express";
import { AccountTypesController } from "../../controllers/growAccounting/_account_typesController";
import * as auth from '../../authService'
import * as cors from 'cors'

export class AccountTypesRoutes { 
    
    public objController: AccountTypesController = new AccountTypesController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/_account_types')
        .post(auth,this.objController.insertAccountType)

        app.route('/_account_types/get')
        .post(auth,this.objController.getAccountTypes)

        app.route('/_account_types/getById')
        .post(auth,this.objController.getAccountTypesById)

        app.route('/_account_types/update')
        .post(auth,this.objController.updateAccountType)

        app.route('/_account_types/delete')
        .post(auth,this.objController.deleteAccountType)
        
    }
    
}