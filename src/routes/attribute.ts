
import {Request, Response, NextFunction} from "express";
import { AttributeController } from "../controllers/attributeController";
import * as auth from '../authService'
import * as cors from 'cors'
export class AttributeRoutes { 
    
    public attributeController: AttributeController = new AttributeController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/attribute')
        .post(auth,this.attributeController.insertAttribute)

        app.route('/attribute/get')
        .post(auth,this.attributeController.getAttribute)

        app.route('/attribute/getById')
        .post(auth,this.attributeController.getAttributeById)

        app.route('/attribute/getByAttributeSet')
        .post(auth,this.attributeController.getAttributeByAttributeSet)

        app.route('/attribute/update')
        .post(auth,this.attributeController.updateAttribute)

        app.route('/attribute/delete')
        .post(auth,this.attributeController.deleteAttribute)
        
    }
    
}