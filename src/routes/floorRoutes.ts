
import {Request, Response, NextFunction} from "express";
import { FloorController } from "../controllers/floorController";
import * as auth from '../authService'
var cors = require('cors');
export class FloorRoutes { 
    
    public floorController: FloorController = new FloorController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/floor')
        .post(auth,this.floorController.insertFloor)

        app.route('/floor/get')
        .post(auth,this.floorController.getFloor)

        app.route('/floor/getById')
        .post(auth,this.floorController.getFloorById)

        app.route('/floor/getWithPlace')
        .post(auth,this.floorController.getFloorsWithPlace)

        app.route('/floor/update')
        .post(auth,this.floorController.updateFloor)

        app.route('/floor/delete')
        .post(auth,this.floorController.deleteFloor)
        
    }
    
}