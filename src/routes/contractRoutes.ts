
import {Request, Response, NextFunction} from "express";
import { ContractController } from "../controllers/contractController";
import * as auth from '../authService'
var cors = require('cors');
export class ContractRoutes { 
    
    public contractController: ContractController = new ContractController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/contract')
        .post(auth,this.contractController.insertContract)

        app.route('/contract/get')
        .post(auth,this.contractController.getContract)

        app.route('/contract/getById')
        .post(auth,this.contractController.getContractById)

        app.route('/contract/getByPlate')
        .post(auth,this.contractController.getContractByPlate)

        app.route('/contract/update')
        .post(auth,this.contractController.updateContract)

        app.route('/contract/delete')
        .post(auth,this.contractController.deleteContract)
        
    }
    
}