export class NiffConceptsGrowAccounting{
    public id_niif_concept: string;
    public id_father: string;
    public description: string;
    public state: string;
}