import { BanksGrowAccounting } from './_banks';
import { AccountsGrowAccounting } from './_accounts';

export class AccountsBanksGrowAccounting{
    public account_number: string;
    public id_bank: BanksGrowAccounting;
    public id_account: AccountsGrowAccounting;
    public note: string;
}