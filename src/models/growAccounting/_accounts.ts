import { AccountTypesGrowAccounting } from './_account_types';
import { BranchOfficesGrowAccounting } from './_branch-offices';
import { NiffConceptsGrowAccounting } from './_niif_concepts';
import { CostCentersGrowAccounting } from './_cost_centers';

export class AccountsGrowAccounting{
    public id_account: string;
    public id_father: string;
    public id_account_type: AccountTypesGrowAccounting;
    public id_branch_office: BranchOfficesGrowAccounting;
    public id_niif_concept : NiffConceptsGrowAccounting;
    public id_cost_center: CostCentersGrowAccounting;
    public minimum_base: number;
    public allows_transact: string;
    public description: string;
    public sign: string;
    public state: string;
}