import { AttributesGrowAccounting } from './_attributes';

export class MovementsAttributesGrowAccounting{
    public id_movement_attributes: string;
    public id_movement_row: string;
    public id_attribute: AttributesGrowAccounting;
    public note: string;
}