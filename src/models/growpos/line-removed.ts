export class LineRemovedGrowPos {
  removeddate: Date;
  name: string;
  ticketid: string;
  productid: string;
  productname: string;
  units: number;
}