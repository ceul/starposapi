import { ProductGrowPos } from "./product";

export class OrderProductsGrowPos{
    public id: string;
    public order: string;
    public note: string;
    public quantity: number;
    public product: ProductGrowPos
}