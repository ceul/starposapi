export class CloseCashGrowPos {
  money: string;
  host: string;
  hostsequence: number;
  datestart: Date;
  dateend: Date;
  nosales: number;
}