import { OrderStateEnum } from "./order-state.enum";
import { OrderTypeEnum } from "./order-type.enum";
import { OrderProductsGrowPos } from "./order-products";

export class OrderGrowPos{
    public id: number;
    public state: OrderStateEnum;
    public type: OrderTypeEnum;
	public start_date: Date;
	public end_date: Date;
	public user: string;
    public external_id: string;
    public products: OrderProductsGrowPos[]
}