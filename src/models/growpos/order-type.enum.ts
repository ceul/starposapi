export enum OrderTypeEnum {
    TABLE = 1,
    DELIVERY = 2,
    BAR = 3,
}