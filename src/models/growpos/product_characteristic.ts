import { AttributeValueGrowPos } from "./attribute-value";

export class ProductCharacteristicGrowPos {
    id_characteristic: string;
    id_product: string;
    val: string;
}