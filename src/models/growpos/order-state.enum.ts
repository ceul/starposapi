export enum OrderStateEnum {
    PROCESS = 1,
    READY = 2,
    ON_DELIVERY = 3,
    FINISH = 4,
}