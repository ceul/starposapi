export class ParkinMovGrowPos {
  id: string;
  places: string;
  plate: number;
  receipt: number;
  date_arrival: Date;
  date_departure: Date;
  type: string;
  fee: string;
  note: string;
  state: number;
}