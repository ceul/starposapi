export enum TicketEnum {
    RECEIPT_NORMAL = 0,
    RECEIPT_REFUND = 1,
    RECEIPT_PAYMENT = 2,
    RECEIPT_NOSALE = 3,
    
// contain partial/full refunds    
    REFUND_NOT = 0, // is a non-refunded ticket    
    REFUND_PARTIAL = 1,
    REFUND_ALL = 2,
}