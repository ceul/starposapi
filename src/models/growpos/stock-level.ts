export class StockLevelGrowPos {
    public id: string;
    public location: string;
    public product: string;
    public stocksecurity: number;
    public stockmaximum: number;
}