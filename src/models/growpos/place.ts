export class PlaceGrowPos {
    public id: string;
    public name: string;
    public seats: string;
    public x: number;
    public y: number;
    public floor: string;
    public customer: string;
    public waiter: string;
    public ticketid: string;
    public tablemoved: number;
    public width: number;
    public height: number;
    public guests: number;
    public occupied: Date;
    public type: string;
}