export class ResourceGrowPos{
    public id: string;
    public name: string;
    public restype: number;
    public content: Buffer;
}