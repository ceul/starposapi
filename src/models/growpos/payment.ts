export class PaymentGrowPos {
  public id: string;
  public receipt: string;
  public payment: string;
  public total: number;
  public tip: number;
  public transid: string;
  public isprocessed: boolean;
  public returnmsg: string;
  public notes: string;
  public tendered: number;
  public cardname: string;
  public voucher: string;
}