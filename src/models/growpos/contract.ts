export class ContractGrowPos {
  id: string;
  customer: string;
  datestart: Date;
  price: number;
  vehicle: string;
  state: number;
}