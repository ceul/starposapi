import { TaxGrowPos } from "./tax";

export class TaxLineGrowPos {
    public tax: TaxGrowPos;
    public base: number;
    public amount: number;
}