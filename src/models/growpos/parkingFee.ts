import { CategoryGrowPos } from "./category";
import { ProductGrowPos } from "./product";

export class ParkingFeeGrowPos {
    public id: string;
    public type: CategoryGrowPos;
	public rate: ProductGrowPos;
	public starttime: Date;
	public endtime: Date;
	public graceperiod: number;
	public time: number;
	public isMonthly: boolean;
}