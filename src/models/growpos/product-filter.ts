export class ProductFilterGrowPos{
    public barcode: string;
    public category: string;
    public name: {
        filter: number,
        value: string
    };
    public pricebuy: {
        filter: number,
        value: number
    }
    public pricesell: {
        filter: number,
        value: number
    }
}