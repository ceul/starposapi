export class VehicleGrowPos {
    id: string;
	plate: string;
	model: string;
	color: string;
	description: string;
	type:string
}