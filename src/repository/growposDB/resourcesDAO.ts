
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { ResourceGrowPos } from '../../models/growpos/resource';
import * as multer from "multer"

export class ResourceDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertResource(resource: ResourceGrowPos) {
        try {
            if (typeof resource.content === "object") {
                resource.content = Buffer.from(JSON.stringify(resource.content));
            }
            let id = uuid.v4();
            resource.id = id;
            resource.restype = 0;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO resources SET ?', [resource]);
            con.release()
            resource.content = JSON.parse(resource.content.toString());
            return resource
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.insertResource.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getResource() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM resources;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.getResource.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getResourceById(resourceId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        content
                        FROM resources
                        WHERE ID = ?;`, [resourceId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.getResourceById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getResourceByHost(resourceName: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        content
                        FROM resources
                        WHERE name = ?;`, [resourceName]);
            if (query.length > 0) {
                query[0]['content'] = JSON.parse(query[0]['content'].toString());
            }
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.getResourceByHost.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCompanyLogo() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        content
                        FROM resources
                        WHERE name = ?;`, ['logo']);

            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.getResourceByHost.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateResource(resource: ResourceGrowPos) {
        try {
            if (typeof resource.content === "object") {
                resource.content = Buffer.from(JSON.stringify(resource.content));
            }
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE resources SET
                        name = ?,
                        content = ?
                        WHERE id = ?;`,
                [resource.name,
                resource.content,
                resource.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.updateResource.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateCompanyLogo(img: Buffer) {
        try {
            let resource = new ResourceGrowPos
            resource.content = img
            resource.name = 'logo'
            resource.restype = 0
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE resources SET
                        content = ?
                        WHERE name = ?;`,
                [resource.content,
                resource.name]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.uploadCompanyLogo.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async uploadCompanyLogo(img: Buffer) {
        try {
            let id = uuid.v4();
            let resource = new ResourceGrowPos
            resource.id = id;
            resource.content = img
            resource.name = 'logo'
            resource.restype = 0
            let con = await this.connection.getConnection()
            let query = await con.query('SELECT id FROM resources WHERE name = ?', [resource.name]);
            let query1 = null
            if (query.length > 0) {
                query1 = await con.query(`UPDATE resources SET
                content = ?
                WHERE name = ?;`,
                    [resource.content,
                    resource.name]);
            } else {
                query1 = await con.query('INSERT INTO resources SET ?', [resource]);
            }
            con.release()
            return query1
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.uploadCompanyLogo.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteResource(resourceId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM resources 
                        WHERE id = ?;`, [resourceId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ResourceDAOGrowPos.name} -> ${this.deleteResource.name}: ${error}`)
            throw new Error(error)
        }
    }
}
