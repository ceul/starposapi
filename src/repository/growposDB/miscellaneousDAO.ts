
import { DataBaseService } from '../../dataBaseService';
import * as mysql from "mysql";
import * as uuid from "uuid";
import * as java from "java.io";
import * as util from 'util';
import { SharedTicketGrowPos } from "models/growpos/sharedticket";
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
var InputObjectStream = java.InputObjectStream;
export class GrowPosMiscellaneousDAO {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async getRootCatergories() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                ID, 
                NAME, 
                IMAGE, 
                TEXTTIP, 
                CATSHOWNAME, 
                CATORDER                     
                FROM categories 
                WHERE PARENTID IS NULL AND CATSHOWNAME = TRUE
                ORDER BY CATORDER, NAME;`);
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getRootCatergories.name}: ${error}`)
        }
    }

    public async getSubCatergories(parentid: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
                ID, 
                NAME, 
                IMAGE, 
                TEXTTIP, 
                CATSHOWNAME,  
                CATORDER                     
                FROM categories WHERE PARENTID = ? 
                ORDER BY CATORDER, NAME;`, [parentid]);
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getSubCatergories.name}: ${error}`)
        }
    }

    public async getProductCatalog(parentid: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
                P.ID, 
                P.REFERENCE, 
                P.CODE, 
                P.CODETYPE, 
                P.NAME, 
                P.PRICEBUY, 
                P.PRICESELL, 
                P.CATEGORY, 
                P.TAXCAT, 
                P.ATTRIBUTESET_ID, 
                P.STOCKCOST, 
                P.STOCKVOLUME,                         
                P.ISCOM, 
                P.ISSCALE, 
                P.ISCONSTANT, 
                P.PRINTKB, 
                P.SENDSTATUS, 
                P.ISSERVICE, 
                P.ATTRIBUTES, 
                P.DISPLAY, 
                P.ISVPRICE, 
                P.ISVERPATRIB, 
                P.TEXTTIP, 
                P.WARRANTY, 
                P.STOCKUNITS,  
                P.PRINTTO, 
                P.SUPPLIER,         
                P.UOM, 
                P.FLAG, 
                P.DESCRIPTION, 
                P.SHORT_DESCRIPTION, 
                P.WEIGTH, 
                P.WIDTH,  
                P.LENGTH, 
                P.HEIGHT 
		        FROM products P, products_cat O 
                WHERE P.ID = O.PRODUCT AND P.CATEGORY = ?  
                ORDER BY O.CATORDER, P.NAME;`, [parentid]);
            return query
            // P.IMAGE,
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getProductCatalog.name}: ${error}`)
        }
    }

    public getSharedTicket(place: string) {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT 
                id, 
                name, 
                appuser, 
                pickupid, 
                locked, 
                products
		        FROM sharedtickets
                WHERE id = '${place}';`, (err, rows) => {
                            if (err)
                                return reject(err);
                            resolve(rows);
                        });
                });
            });
            // P.IMAGE,
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getSharedTicket.name}: ${error}`)
        }
    }

    public getTaxes() {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT 
                id, 
		        name, 
		        category, 
		        rate,
		        ratecascade
		        FROM taxes;`, (err, rows) => {
                            if (err)
                                return reject(err);
                            resolve(rows);
                        });
                });
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getTaxes.name}: ${error}`)
        }
    }

    public insertSharedTicket(sharedticket: SharedTicketGrowPos) {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`INSERT INTO sharedtickets (ID, 
                NAME, 
                PRODUCTS, 
                APPUSER, 
                PICKUPID) 
                VALUES ('${sharedticket.id}', 
                '${sharedticket.name}', 
                '${JSON.stringify(sharedticket.products)}', 
                '${sharedticket.appuser}', 
                '${sharedticket.pickupid}');`, (err, rows) => {
                            if (err)
                                return reject(err);
                            resolve(rows);
                        });
                });
            });

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.insertSharedTicket.name}: ${error}`)
        }
    }

    public updateSharedTicket(sharedticket: SharedTicketGrowPos) {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`UPDATE sharedtickets SET 
                NAME = '${sharedticket.name}', 
                PRODUCTS = '${JSON.stringify(sharedticket.products)}', 
                APPUSER = '${sharedticket.appuser}', 
                PICKUPID = '${sharedticket.pickupid}' 
                WHERE ID = '${sharedticket.id}';`, (err, rows) => {
                            if (err)
                                return reject(err);
                            resolve(rows);
                        });
                });
            });

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.updateSharedTicket.name}: ${error}`)
        }
    }

    public deleteSharedTicket(sharedticket: SharedTicketGrowPos) {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`DELETE FROM sharedtickets 
                WHERE ID = ${sharedticket.id}';`, (err, rows) => {
                            if (err)
                                return reject(err);
                            resolve(rows);
                        });
                });
            });

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.deleteSharedTicket.name}: ${error}`)
        }
    }

    public getUsers() {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT ID, NAME FROM people;`, (err, rows) => {
                        if (err)
                            return reject(err);
                        resolve(rows);
                    });
                });
            });

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getUsers.name}: ${error}`)
        }
    }

    public getFloors() {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT ID, NAME FROM floors;`, (err, rows) => {
                        if (err)
                            return reject(err);
                        resolve(rows);
                    });
                });
            });

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getFloors.name}: ${error}`)
        }
    }

    public getPlaces(floor: string) {
        try {
            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT ID, NAME FROM places WHERE FLOOR = ?;`,[floor], (err, rows) => {
                        if (err)
                            return reject(err);
                        resolve(rows);
                    });
                });
            });

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getPlaces.name}: ${error}`)
        }
    }

    /*------------------------------------------------ woocommerce ------------------------------------------ */
    public insertExternalProduct(local_product: string, external_product: string) {
        try {
            let id = uuid.v4();
            this.connection.getConnection((err, con) => {
                return con.query(`INSERT INTO extern_product (ID, platform_id, local_id ,extern_id) 
                            VALUES ('${id}', '6c84fb90-12c4-11e1-840d-7b25c5ee775a', '${local_product}','${external_product}');`, function (err, rows) {
                        if (err) {
                            console.log('Error insertion: ', err, `${GrowPosMiscellaneousDAO.name} -> `);//${this.insertExternalProduct.name}`);
                            //return res.json({'error': true, 'message': 'Error occurred'+err});
                        };

                    });
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.insertExternalProduct.name}: ${error}`)
        }
    }

    public deleteProduct(local_product: string, web_sell: boolean) {
        try {
            let id = uuid.v4();
            this.connection.getConnection((err, con) => {
                con.beginTransaction(function (err) {
                    if (err) { throw err; }
                    con.query(`DELETE FROM products_cat WHERE PRODUCT = '${local_product}');`, function (err, rows) {
                        if (err) {
                            con.rollback(function () {
                                throw err;
                            });
                            console.log('Error delete' + err + ` ${GrowPosMiscellaneousDAO.name} -> ${this.deletelProduct.name}`);
                            //return res.json({'error': true, 'message': 'Error occurred'+err});
                        }
                    });
                    if (web_sell) {
                        con.query(`DELETE FROM extern_product WHERE local_id = '${local_product}');`, function (err, rows) {
                            if (err) {
                                con.rollback(function () {
                                    throw err;
                                });
                                console.log('Error delete' + err + ` ${GrowPosMiscellaneousDAO.name} -> ${this.deletelProduct.name}`);
                                //return res.json({'error': true, 'message': 'Error occurred'+err});
                            }
                        });
                    }
                    con.query(`DELETE FROM products WHERE ID = '${local_product}');`, function (err, rows) {
                        if (err) {
                            con.rollback(function () {
                                throw err;
                            });
                            console.log('Error delete' + err + ` ${GrowPosMiscellaneousDAO.name} -> ${this.deletelProduct.name}`);
                            //return res.json({'error': true, 'message': 'Error occurred'+err});
                        }
                    });
                    con.commit(function (err) {
                        if (err) {
                            con.rollback(function () {
                                throw err;
                            });
                        }
                        
                        con.end();
                    });
                });
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.deleteProduct.name}: ${error}`)
        }
    }

    public getExternProduct(local_product: string) {
        try {

            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT extern_id FROM extern_product WHERE local_id = '${local_product}';`, (err, rows) => {
                        if (err)
                            return reject(err);
                        if (rows.length > 0) {
                            resolve(rows[0].extern_id);
                        } else {
                            resolve(rows);
                        }
                    });
                });
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getExternProduct.name}: ${error}`)
        }
    }

    /**
	 * Last modification: 18-09-2018 22:13
	 * @description This method select the product's category 
	 * @param local_category : string, is the product category
	 * @returns {Promise}
	 * @author Carlos Urrego
	 */
    public getProductExternCategory(local_category: string) {
        try {

            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT extern_id FROM extern_category WHERE local_id = '${local_category}';`, (err, rows) => {
                        if (err)
                            return reject(err);
                        resolve(rows);
                    });
                });
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getProductExternCategory.name}: ${error}`)
        }
    }

    /**
	 * Last modification: 18-09-2018 22:13
	 * @description This method select the product's category 
	 * @param category : string, is the product category
	 * @returns {Promise}
	 * @author Carlos Urrego
	 */
    public getCategory(category: string) {
        try {

            return new Promise((resolve, reject) => {
                this.connection.getConnection((err, con) => {
                    return con.query(`SELECT * FROM categories WHERE id = '${category}';`, (err, rows) => {
                        if (err)
                            return reject(err);
                        if (rows.length > 0) {
                            resolve(rows[0]);
                        } else {
                            resolve(rows);
                        }
                    });
                });
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.getCategory.name}: ${error}`)
        }
    }

    public insertExternalCategory(local_category: string, external_category: string) {
        try {
            let id = uuid.v4();
            this.connection.getConnection((err, con) => {
                return con.query(`INSERT INTO extern_category (ID, platform_id, local_id ,extern_id) 
                            VALUES ('${id}', '6c84fb90-12c4-11e1-840d-7b25c5ee775a', '${local_category}','${external_category}');`, function (err, rows) {
                        if (err) {
                            console.log('Error insertion: ', err, `${GrowPosMiscellaneousDAO.name} -> `);//${this.insertExternalProduct.name}`);
                            //return res.json({'error': true, 'message': 'Error occurred'+err});
                        };
                    }
                );
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${GrowPosMiscellaneousDAO.name} -> ${this.insertExternalCategory.name}: ${error}`)
        }
    }
}
