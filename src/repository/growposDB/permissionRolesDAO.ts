//    StarPos
//    Copyright (c) 2020 GrowPos
//    http://starpos.co
//
//    This file is part of StarPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { PermissionsRoleGrowPos } from '../../models/growpos/permissions_roles';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class PermissionsRolesDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async getPermisionRole() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                            id_permission,
                            id_rol
                        FROM permissions_roles;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PermissionsRolesDAOGrowPos.name} -> ${this.getPermisionRole.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getPermisionRoleById(roleId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                            id_permission,
                            id_rol
                        FROM permissions_roles
                        WHERE id_rol = ? ORDER BY id_permission;`, [roleId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PermissionsRolesDAOGrowPos.name} -> ${this.getPermisionRoleById.name}: ${error}`)
            throw new Error(error)
        }
    }
}
