
import { DataBaseService } from '../../dataBaseService';
import * as mysql from "mysql";
import * as uuid from "uuid";
import * as java from "java.io";
import * as util from 'util';
import { CustomerGrowPos } from 'models/growpos/customer';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
var InputObjectStream = java.InputObjectStream;
export class CustomerDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertCustomer(customer: CustomerGrowPos) {
        try {
            let id = uuid.v4();
            customer.id = id;
            if(customer.taxcategory === ''){
                customer.taxcategory = null
            }
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO third_party SET ?', customer);
            con.release();
            return customer
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.insertCustomer.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCustomers() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        searchkey,
                        taxid,
                        name ,
                        taxcategory,
                        card,
                        maxdebt,
                        address,
                        address2,
                        postal,
                        city,
                        region,
                        country,
                        firstname,
                        lastname,
                        email,
                        phone,
                        phone2,
                        fax,
                        notes,
                        visible,
                        curdate,
                        curdebt,
                        isvip,
                        discount,
                        id_third_party_type
                        FROM third_party;`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.getCustomers.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCustomerById(customerId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        searchkey,
                        taxid,
                        name ,
                        taxcategory,
                        card,
                        maxdebt,
                        address,
                        address2,
                        postal,
                        city,
                        region,
                        country,
                        firstname,
                        lastname,
                        email,
                        phone,
                        phone2,
                        fax,
                        notes,
                        visible,
                        curdate,
                        curdebt,
                        isvip,
                        discount,
                        id_third_party_type
                        FROM third_party
                        WHERE ID = ?;`, [customerId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.getCustomerById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateCustomer(customer: CustomerGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE third_party SET
                        searchkey = ?,
                        taxid = ?,
                        name = ? ,
                        taxcategory = ?,
                        card = ?,
                        maxdebt = ?,
                        address = ?,
                        address2 = ?,
                        postal = ?,
                        city = ?,
                        region = ?,
                        country = ?,
                        firstname = ?,
                        lastname = ?,
                        email = ?,
                        phone = ?,
                        phone2 = ?,
                        fax = ?,
                        notes = ?,
                        visible = ?,
                        curdate = ?,
                        curdebt = ?,
                        isvip = ?,
                        discount = ?
                        WHERE id = ?;`,
                [customer.searchkey,
                customer.taxid,
                customer.name,
                customer.taxcategory,
                customer.card,
                customer.maxdebt,
                customer.address,
                customer.address2,
                customer.postal,
                customer.city,
                customer.region,
                customer.country,
                customer.firstname,
                customer.lastname,
                customer.email,
                customer.phone,
                customer.phone2,
                customer.fax,
                customer.notes,
                customer.visible,
                customer.curdate,
                customer.curdebt,
                customer.isvip,
                customer.discount,
                customer.id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.updateCustomer.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteCustomer(customerId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM third_party 
                        WHERE id = ?;`, [customerId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.deleteCustomer.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async debtUpdate(customer: CustomerGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                let con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE third_party SET CURDEBT = ?, CURDATE = ? WHERE ID = ?;`, [customer.curdebt, new Date(), customer.id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.debtUpdate.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTransactions(customerId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT tickets.ticketid AS ticket,
            receipts.datenew AS date,
             products.NAME AS product,
             SUM(ticketlines.UNITS) AS units,
             SUM(ticketlines.UNITS * ticketlines.PRICE) AS amount,
             SUM(ticketlines.UNITS * ticketlines.PRICE * (1.0 + taxes.RATE)) AS total
             FROM ((((ticketlines ticketlines CROSS JOIN taxes taxes ON (ticketlines.TAXID = taxes.ID)) INNER JOIN tickets tickets ON 
             (tickets.ID = ticketlines.TICKET)) INNER JOIN third_party third_party ON (third_party.ID = tickets.CUSTOMER)) INNER JOIN receipts receipts ON 
             (tickets.ID = receipts.ID)) LEFT OUTER JOIN products products ON (ticketlines.PRODUCT = products.ID) WHERE tickets.CUSTOMER = ? GROUP BY third_party.ID,
             date,
             tickets.ticketid,
             products.NAME,
             tickets.TICKETTYPE ORDER BY date DESC`, [customerId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.getTransactions.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCustomerFiltered(filter: CustomerGrowPos) {
        try {
            let params = []
            let sql = `SELECT
            id,
            searchkey,
            taxid,
            name ,
            taxcategory,
            card,
            maxdebt,
            address,
            address2,
            postal,
            city,
            region,
            country,
            firstname,
            lastname,
            email,
            phone,
            phone2,
            fax,
            notes,
            visible,
            curdate,
            curdebt,
            isvip,
            discount,
            id_third_party_type
            FROM third_party WHERE (1=1`
            if (filter.name !== undefined) {
                if (filter.name !== '') {
                    sql += ' and name = ?'
                    params.push(filter.name)
                }
            }
            if (filter.searchkey !== undefined) {
                if (filter.searchkey !== '') {
                    sql += ' and searchkey = ?'
                    params.push(filter.searchkey)
                }
            }
            if (filter.email !== undefined) {
                if (filter.email !== '') {
                    sql += ' and email = ?'
                    params.push(filter.email)
                }
            }
            if (filter.phone !== undefined) {
                if (filter.phone !== '') {
                    sql += ' and phone = ?'
                    params.push(filter.phone)
                }
            }
            if (filter.postal !== undefined) {
                if (filter.postal !== '') {
                    sql += ' and postal = ?'
                    params.push(filter.postal)
                }
            }
            sql += ') ORDER BY name'
            let con = await this.connection.getConnection()
            let query = await con.query(sql, params)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CustomerDAOGrowPos.name} -> ${this.getCustomerFiltered.name}: ${error}`)
            throw new Error(error)
        }
    }

}
