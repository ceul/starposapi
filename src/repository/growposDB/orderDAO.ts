
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { OrderGrowPos } from '../../models/growpos/order';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { OrderStateEnum } from '../../models/growpos/order-state.enum';
import * as _ from "lodash"
import { SocketService } from '../../socketService'
const axios = require('axios');

export class OrderDAOGrowPos {

    private log
    private connection;
    private socket: SocketService
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
        this.socket = SocketService.getInstance()
    }

    public async insertOrder(order: OrderGrowPos) {
        try {
            order.id = uuid.v4();
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                let query = await con.query(`INSERT INTO orders (id, 
                state, 
                type, 
                start_date, 
                end_date, 
                user, 
                external_id) VALUES (?,?,?,?,?,?,?)`, [order.id,
                    OrderStateEnum.PROCESS,
                    order.type,
                    order.start_date,
                    order.end_date,
                    order.user,
                    order.external_id]);
                let promises = []
                order.products.forEach(product => {
                    let id = uuid.v4()
                    promises.push(con.query(`INSERT INTO rel_product_order (id, 
                        product, 
                        order, 
                        qty,
                        note) VALUES (?,?,?,?,?)`, [id,
                            product.product,
                            product.order,
                            product.note,
                            product.quantity]))
                })
                await Promise.all(promises)
                con.release()
                let socket = SocketService.getInstance()
                socket.emit('order', order)
                return order
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${OrderDAOGrowPos.name} -> ${this.insertOrder.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getOnProcessOrders() {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                orders.id as id, 
                orders.state as state, 
                orders.type as type, 
                orders.start_date as start_date, 
                orders.end_date as end_date, 
                orders.user as user, 
                orders.external_id as external_id,
                rel_product_order.note as note,
                rel_product_order.qty as quantity,
                products.id as productid,
                products.reference as productreference,
                products.code as productcode,
                products.codetype as productcodetype,
                products.name as productname,
                products.pricebuy as productpricebuy,
                products.pricesell as productpricesell,
                products.category as productcategory,
                products.taxcat as producttaxcat,
                products.attributeset_id as productattributeset_id,
                products.stockcost as productstockcost,
                products.stockvolume as productstockvolume,
                products.iscom as productiscom,
                products.isscale as productisscale,
                products.isconstant as productisconstant,
                products.printkb as productprintkb,
                products.sendstatus as productsendstatus,
                products.isservice as productisservice,
                products.attributes as productattributes,
                products.display as productdisplay,
                products.isvprice as productisvprice,
                products.isverpatrib as productisverpatrib,
                products.texttip as producttexttip,
                products.warranty as productwarranty,
                products.stockunits as productstockunits,
                products.printto as productprintto,
                products.supplier as productsupplier,
                products.uom as productuom,
                products.flag as productflag,
                products.description as productdescription,
                products.short_description as productshort_description,
                products.weigth as productweigth,
                products.length as productlength,
                products.height as productheight,
                products.width as productwidth
                FROM ((orders INNER JOIN rel_product_order 
                ON orders.id = rel_product_order.order) 
                INNER JOIN products ON rel_product_order.product = products.id)
                WHERE state = '1';`);
            if (query.length > 0) {
                result = _.chain(query).groupBy("id").map((id) => {
                    return {
                        name: _.get(_.find(id, 'id'), 'id'),
                        id: _.get(_.find(id, 'id'), 'id'),
                        state: _.get(_.find(id, 'state'), 'state'),
                        type: _.get(_.find(id, 'type'), 'type'),
                        start_date: _.get(_.find(id, 'start_date'), 'start_date'),
                        end_date: _.get(_.find(id, 'end_date'), 'end_date'),
                        user: _.get(_.find(id, 'user'), 'user'),
                        external_id: _.get(_.find(id, 'external_id'), 'external_id'),
                        products: _.map(id, (item) => {
                            return {
                                note: item['note'],
                                quantity: item['quantity'],
                                product: {
                                    id: item['productid'],
                                    reference: item['productreference'],
                                    code: item['productcode'],
                                    codetype: item['productcodetype'],
                                    name: item['productname'],
                                    pricebuy: item['productpricebuy'],
                                    pricesell: item['productpricesell'],
                                    category: item['productcategory'],
                                    taxcat: item['producttaxcat'],
                                    attributeset_id: item['productattributeset_id'],
                                    stockcost: item['productstockcost'],
                                    stockvolume: item['productstockvolume'],
                                    iscom: item['productiscom'],
                                    isscale: item['productisscale'],
                                    isconstant: item['productisconstant'],
                                    printkb: item['productprintkb'],
                                    sendstatus: item['productsendstatus'],
                                    isservice: item['productisservice'],
                                    attributes: item['productattributes'],
                                    display: item['productdisplay'],
                                    isvprice: item['productisvprice'],
                                    isverpatrib: item['productisverpatrib'],
                                    texttip: item['producttexttip'],
                                    warranty: item['productwarranty'],
                                    stockunits: item['productstockunits'],
                                    printto: item['productprintto'],
                                    supplier: item['productsupplier'],
                                    uom: item['productuom'],
                                    flag: item['productflag'],
                                    description: item['productdescription'],
                                    short_description: item['productshort_description'],
                                    weigth: item['productweigth'],
                                    length: item['productlength'],
                                    height: item['productheight'],
                                    width: item['productwidth'],
                                }
                            }
                        })
                    }
                }).value();
            }
            con.release()
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${OrderDAOGrowPos.name} -> ${this.getOnProcessOrders.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getReadyToDeliver() {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                orders.id as id, 
                orders.state as state, 
                orders.type as type, 
                orders.start_date as start_date, 
                orders.end_date as end_date, 
                orders.user as user, 
                orders.external_id as external_id,
                rel_product_order.note as note,
                rel_product_order.qty as quantity,
                products.id as productid,
                products.reference as productreference,
                products.code as productcode,
                products.codetype as productcodetype,
                products.name as productname,
                products.pricebuy as productpricebuy,
                products.pricesell as productpricesell,
                products.category as productcategory,
                products.taxcat as producttaxcat,
                products.attributeset_id as productattributeset_id,
                products.stockcost as productstockcost,
                products.stockvolume as productstockvolume,
                products.iscom as productiscom,
                products.isscale as productisscale,
                products.isconstant as productisconstant,
                products.printkb as productprintkb,
                products.sendstatus as productsendstatus,
                products.isservice as productisservice,
                products.attributes as productattributes,
                products.display as productdisplay,
                products.isvprice as productisvprice,
                products.isverpatrib as productisverpatrib,
                products.texttip as producttexttip,
                products.warranty as productwarranty,
                products.stockunits as productstockunits,
                products.printto as productprintto,
                products.supplier as productsupplier,
                products.uom as productuom,
                products.flag as productflag,
                products.description as productdescription,
                products.short_description as productshort_description,
                products.weigth as productweigth,
                products.length as productlength,
                products.height as productheight,
                products.width as productwidth
                FROM ((orders INNER JOIN rel_product_order 
                ON orders.id = rel_product_order.order) 
                INNER JOIN products ON rel_product_order.product = products.id)
                WHERE state <> '1' AND state <> '4';`);
            if (query.length > 0) {
                result = _.chain(query).groupBy("id").map((id) => {
                    return {
                        name: _.get(_.find(id, 'id'), 'id'),
                        id: _.get(_.find(id, 'id'), 'id'),
                        state: _.get(_.find(id, 'state'), 'state'),
                        type: _.get(_.find(id, 'type'), 'type'),
                        start_date: _.get(_.find(id, 'start_date'), 'start_date'),
                        end_date: _.get(_.find(id, 'end_date'), 'end_date'),
                        user: _.get(_.find(id, 'user'), 'user'),
                        external_id: _.get(_.find(id, 'external_id'), 'external_id'),
                        products: _.map(id, (item) => {
                            return {
                                note: item['note'],
                                quantity: item['quantity'],
                                product: {
                                    id: item['productid'],
                                    reference: item['productreference'],
                                    code: item['productcode'],
                                    codetype: item['productcodetype'],
                                    name: item['productname'],
                                    pricebuy: item['productpricebuy'],
                                    pricesell: item['productpricesell'],
                                    category: item['productcategory'],
                                    taxcat: item['producttaxcat'],
                                    attributeset_id: item['productattributeset_id'],
                                    stockcost: item['productstockcost'],
                                    stockvolume: item['productstockvolume'],
                                    iscom: item['productiscom'],
                                    isscale: item['productisscale'],
                                    isconstant: item['productisconstant'],
                                    printkb: item['productprintkb'],
                                    sendstatus: item['productsendstatus'],
                                    isservice: item['productisservice'],
                                    attributes: item['productattributes'],
                                    display: item['productdisplay'],
                                    isvprice: item['productisvprice'],
                                    isverpatrib: item['productisverpatrib'],
                                    texttip: item['producttexttip'],
                                    warranty: item['productwarranty'],
                                    stockunits: item['productstockunits'],
                                    printto: item['productprintto'],
                                    supplier: item['productsupplier'],
                                    uom: item['productuom'],
                                    flag: item['productflag'],
                                    description: item['productdescription'],
                                    short_description: item['productshort_description'],
                                    weigth: item['productweigth'],
                                    length: item['productlength'],
                                    height: item['productheight'],
                                    width: item['productwidth'],
                                }
                            }
                        })
                    }
                }).value();
            }
            con.release()
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${OrderDAOGrowPos.name} -> ${this.getReadyToDeliver.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getOrder() {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                orders.id as id, 
                orders.state as state, 
                orders.type as type, 
                orders.start_date as start_date, 
                orders.end_date as end_date, 
                orders.user as user, 
                orders.external_id as external_id,
                rel_product_order.note as note,
                rel_product_order.qty as quantity,
                products.id as productid,
                products.reference as productreference,
                products.code as productcode,
                products.codetype as productcodetype,
                products.name as productname,
                products.pricebuy as productpricebuy,
                products.pricesell as productpricesell,
                products.category as productcategory,
                products.taxcat as producttaxcat,
                products.attributeset_id as productattributeset_id,
                products.stockcost as productstockcost,
                products.stockvolume as productstockvolume,
                products.iscom as productiscom,
                products.isscale as productisscale,
                products.isconstant as productisconstant,
                products.printkb as productprintkb,
                products.sendstatus as productsendstatus,
                products.isservice as productisservice,
                products.attributes as productattributes,
                products.display as productdisplay,
                products.isvprice as productisvprice,
                products.isverpatrib as productisverpatrib,
                products.texttip as producttexttip,
                products.warranty as productwarranty,
                products.stockunits as productstockunits,
                products.printto as productprintto,
                products.supplier as productsupplier,
                products.uom as productuom,
                products.flag as productflag,
                products.description as productdescription,
                products.short_description as productshort_description,
                products.weigth as productweigth,
                products.length as productlength,
                products.height as productheight,
                products.width as productwidth
                FROM ((orders INNER JOIN rel_product_order 
                ON orders.id = rel_product_order.order) 
                INNER JOIN products ON rel_product_order.product = products.id)
                GROUP BY orders.id;`);
            if (query.length > 0) {
                result = _.chain(query).groupBy("id").map((id) => {
                    return {
                        name: _.get(_.find(id, 'id'), 'id'),
                        id: _.get(_.find(id, 'id'), 'id'),
                        state: _.get(_.find(id, 'state'), 'state'),
                        type: _.get(_.find(id, 'type'), 'type'),
                        start_date: _.get(_.find(id, 'start_date'), 'start_date'),
                        end_date: _.get(_.find(id, 'end_date'), 'end_date'),
                        user: _.get(_.find(id, 'user'), 'user'),
                        external_id: _.get(_.find(id, 'external_id'), 'external_id'),
                        products: _.map(id, (item) => {
                            return {
                                note: item['note'],
                                quantity: item['quantity'],
                                product: {
                                    id: item['productid'],
                                    reference: item['productreference'],
                                    code: item['productcode'],
                                    codetype: item['productcodetype'],
                                    name: item['productname'],
                                    pricebuy: item['productpricebuy'],
                                    pricesell: item['productpricesell'],
                                    category: item['productcategory'],
                                    taxcat: item['producttaxcat'],
                                    attributeset_id: item['productattributeset_id'],
                                    stockcost: item['productstockcost'],
                                    stockvolume: item['productstockvolume'],
                                    iscom: item['productiscom'],
                                    isscale: item['productisscale'],
                                    isconstant: item['productisconstant'],
                                    printkb: item['productprintkb'],
                                    sendstatus: item['productsendstatus'],
                                    isservice: item['productisservice'],
                                    attributes: item['productattributes'],
                                    display: item['productdisplay'],
                                    isvprice: item['productisvprice'],
                                    isverpatrib: item['productisverpatrib'],
                                    texttip: item['producttexttip'],
                                    warranty: item['productwarranty'],
                                    stockunits: item['productstockunits'],
                                    printto: item['productprintto'],
                                    supplier: item['productsupplier'],
                                    uom: item['productuom'],
                                    flag: item['productflag'],
                                    description: item['productdescription'],
                                    short_description: item['productshort_description'],
                                    weigth: item['productweigth'],
                                    length: item['productlength'],
                                    height: item['productheight'],
                                    width: item['productwidth'],
                                }
                            }
                        })
                    }
                }).value();
            }
            con.release()
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${OrderDAOGrowPos.name} -> ${this.getOrder.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getOrderById(orderId: string) {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                orders.id as id, 
                orders.state as state, 
                orders.type as type, 
                orders.start_date as start_date, 
                orders.end_date as end_date, 
                orders.user as user, 
                orders.external_id as external_id,
                rel_product_order.note as note,
                rel_product_order.qty as quantity,
                products.id as productid,
                products.reference as productreference,
                products.code as productcode,
                products.codetype as productcodetype,
                products.name as productname,
                products.pricebuy as productpricebuy,
                products.pricesell as productpricesell,
                products.category as productcategory,
                products.taxcat as producttaxcat,
                products.attributeset_id as productattributeset_id,
                products.stockcost as productstockcost,
                products.stockvolume as productstockvolume,
                products.iscom as productiscom,
                products.isscale as productisscale,
                products.isconstant as productisconstant,
                products.printkb as productprintkb,
                products.sendstatus as productsendstatus,
                products.isservice as productisservice,
                products.attributes as productattributes,
                products.display as productdisplay,
                products.isvprice as productisvprice,
                products.isverpatrib as productisverpatrib,
                products.texttip as producttexttip,
                products.warranty as productwarranty,
                products.stockunits as productstockunits,
                products.printto as productprintto,
                products.supplier as productsupplier,
                products.uom as productuom,
                products.flag as productflag,
                products.description as productdescription,
                products.short_description as productshort_description,
                products.weigth as productweigth,
                products.length as productlength,
                products.height as productheight,
                products.width as productwidth
                FROM ((orders INNER JOIN rel_product_order 
                ON orders.id = rel_product_order.order) 
                INNER JOIN products ON rel_product_order.product = products.id)
                WHERE orders.id = ? GROUP BY orders.id;`, [orderId]);
            if (query.length > 0) {
                result = _.chain(query).groupBy("id").map((id) => {
                    return {
                        name: _.get(_.find(id, 'id'), 'id'),
                        id: _.get(_.find(id, 'id'), 'id'),
                        state: _.get(_.find(id, 'state'), 'state'),
                        type: _.get(_.find(id, 'type'), 'type'),
                        start_date: _.get(_.find(id, 'start_date'), 'start_date'),
                        end_date: _.get(_.find(id, 'end_date'), 'end_date'),
                        user: _.get(_.find(id, 'user'), 'user'),
                        external_id: _.get(_.find(id, 'external_id'), 'external_id'),
                        products: _.map(id, (item) => {
                            return {
                                note: item['note'],
                                quantity: item['quantity'],
                                product: {
                                    id: item['productid'],
                                    reference: item['productreference'],
                                    code: item['productcode'],
                                    codetype: item['productcodetype'],
                                    name: item['productname'],
                                    pricebuy: item['productpricebuy'],
                                    pricesell: item['productpricesell'],
                                    category: item['productcategory'],
                                    taxcat: item['producttaxcat'],
                                    attributeset_id: item['productattributeset_id'],
                                    stockcost: item['productstockcost'],
                                    stockvolume: item['productstockvolume'],
                                    iscom: item['productiscom'],
                                    isscale: item['productisscale'],
                                    isconstant: item['productisconstant'],
                                    printkb: item['productprintkb'],
                                    sendstatus: item['productsendstatus'],
                                    isservice: item['productisservice'],
                                    attributes: item['productattributes'],
                                    display: item['productdisplay'],
                                    isvprice: item['productisvprice'],
                                    isverpatrib: item['productisverpatrib'],
                                    texttip: item['producttexttip'],
                                    warranty: item['productwarranty'],
                                    stockunits: item['productstockunits'],
                                    printto: item['productprintto'],
                                    supplier: item['productsupplier'],
                                    uom: item['productuom'],
                                    flag: item['productflag'],
                                    description: item['productdescription'],
                                    short_description: item['productshort_description'],
                                    weigth: item['productweigth'],
                                    length: item['productlength'],
                                    height: item['productheight'],
                                    width: item['productwidth'],
                                }
                            }
                        })
                    }
                }).value();
            }
            con.release()
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${OrderDAOGrowPos.name} -> ${this.getOrderById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateOrder(order: OrderGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE orders SET
                        end_date = ?,
                        state = ?
                        WHERE id = ?;`,
                [order.end_date,
                order.state,
                order.id]);
            con.release()
            let socket = SocketService.getInstance()
            socket.emit('order', order)
            /*if(order.state === OrderStateEnum.FINISH){
                let response = await axios.post('/order/update', order)
                debugger
            }*/
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${OrderDAOGrowPos.name} -> ${this.updateOrder.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteOrder(orderId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM orders 
                        WHERE id = ?;`, [orderId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${OrderDAOGrowPos.name} -> ${this.deleteOrder.name}: ${error}`)
            throw new Error(error)
        }
    }
}
