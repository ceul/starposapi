
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { ContractGrowPos } from '../../models/growpos/contract';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class ContractDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertContract(contract: ContractGrowPos) {
        try {
            let id = uuid.v4();
            contract.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO contract SET ?', contract);
            con.release()
            return contract
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ContractDAOGrowPos.name} -> ${this.insertContract.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getContract() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        contract.id as id,
                        contract.customer as customer,
                        contract.datestart as datestart,
                        contract.price as price,
                        contract.vehicle as vehicle,
                        contract.state as state,
                        vehicles.id as vehicleId ,
                        vehicles.plate as vehiclePlate ,
                        third_party.id as customerId ,
                        third_party.name as customerName 
                        FROM ((contract INNER JOIN vehicles ON (contract.vehicle = vehicles.id)) 
                        INNER JOIN third_party ON (contract.customer = third_party.id)) ;`);
            if (query.length > 0) {
                query.map((item, index)=> {
                    query[index]['vehicle'] = {}
                    query[index]['customer'] = {}
                    query[index]['vehicle']['id'] = item['vehicleId']
                    delete item['vehicleId']
                    query[index]['vehicle']['plate'] = item['vehiclePlate']
                    delete item['vehiclePlate']
                    query[index]['customer']['id'] = item['customerId']
                    delete item['customerId']
                    query[index]['customer']['name'] = item['customerName']
                    delete item['customerName']
                })
            }
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ContractDAOGrowPos.name} -> ${this.getContract.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getContractById(contractId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        customer,
                        datestart,
                        price,
                        vehicle,
                        state
                        FROM contract
                        WHERE ID = ?;`, [contractId]);

            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ContractDAOGrowPos.name} -> ${this.getContractById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getContractByPlate(contractPlate: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        contract.id as id,
                        contract.customer as customer,
                        contract.datestart as datestart,
                        contract.price as price,
                        contract.vehicle as vehicle,
                        contract.state as state,
                        vehicles.id as vehicleId,
                        vehicles.plate as vehiclePlate,
                        vehicles.model as vehicleModel,
                        vehicles.color as vehicleColor,
                        vehicles.description as vehicleDescription, 
                        vehicles.type as vehicleType
                        FROM contract INNER JOIN vehicles ON contract.vehicle = vehicles.id
                        WHERE vehicles.plate = ?;`, [contractPlate]);
                        if (query.length > 0) {
                            query.map((item, index)=> {
                                query[index]['vehicle'] = {}
                                query[index]['vehicle']['id'] = item['vehicleId']
                                delete item['vehicleId']
                                query[index]['vehicle']['plate'] = item['vehiclePlate']
                                delete item['vehiclePlate']
                                query[index]['vehicle']['model'] = item['vehicleModel']
                                delete item['vehicleModel']
                                query[index]['vehicle']['colr'] = item['vehicleColor']
                                delete item['vehicleColor']
                                query[index]['vehicle']['description'] = item['vehicleDescription']
                                delete item['vehicleDescription']
                                query[index]['vehicle']['type'] = item['vehicleType']
                                delete item['vehicleType']
                            })
                        }

            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ContractDAOGrowPos.name} -> ${this.getContractByPlate.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateContract(contract: ContractGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE contract SET
                        customer = ?,
                        datestart = ?,
                        price = ?,
                        vehicle = ?,
                        state = ?
                        WHERE id = ?;`,
                [contract.customer,
                contract.datestart,
                contract.price,
                contract.vehicle,
                contract.state,
                contract.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ContractDAOGrowPos.name} -> ${this.updateContract.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteContract(contractId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM contract 
                        WHERE id = ?;`, [contractId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ContractDAOGrowPos.name} -> ${this.deleteContract.name}: ${error}`)
            throw new Error(error)
        }
    }
}
