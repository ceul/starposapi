
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { TaxGrowPos } from '../../models/growpos/tax';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class TaxDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertTax(tax: TaxGrowPos) {
        try {
            let id = uuid.v4();
            tax.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO taxes SET ?', tax);
            con.release()
            return tax
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${TaxDAOGrowPos.name} -> ${this.insertTax.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTax() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT id, name, category, custcategory, parentid, rate, ratecascade, rateorder FROM taxes ORDER BY name;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${TaxDAOGrowPos.name} -> ${this.getTax.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTaxById(taxId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name, 
                        category, 
                        custcategory, 
                        parentid, 
                        rate, 
                        ratecascade, 
                        rateorder 
                        FROM taxes
                        WHERE ID = ?;`, [taxId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${TaxDAOGrowPos.name} -> ${this.getTaxById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateTax(tax: TaxGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE taxes SET
                        name = ?,
                        category = ?, 
                        custcategory = ?, 
                        parentid = ?, 
                        rate = ?, 
                        ratecascade = ?, 
                        rateorder = ?, 
                        WHERE id = ?;`,
                [tax.name,
                tax.taxcategoryid,
                tax.taxcustcategoryid,
                tax.parentid,
                tax.rate,
                tax.order,
                tax.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${TaxDAOGrowPos.name} -> ${this.updateTax.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteTax(taxId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM taxes 
                        WHERE id = ?;`, [taxId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${TaxDAOGrowPos.name} -> ${this.deleteTax.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTotalTaxesByCloseCash(closeCash: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT SUM(taxlines.AMOUNT) as amount, 
            SUM(taxlines.BASE) as base FROM receipts, 
            taxlines WHERE receipts.ID = taxlines.RECEIPT 
            AND receipts.MONEY = ?;`, [closeCash]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxDAOGrowPos.name} -> ${this.getTotalTaxesByCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTaxesByCategory(category: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
            id,
            name,
            category,
            custcategory,
            parentid,
            rate,
            ratecascade,
            rateorder
            FROM taxes WHERE category = ?;`,[category])
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxDAOGrowPos.name} -> ${this.getTaxesByCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

}
