
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { RoleGrowPos } from '../../models/growpos/role';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class RoleDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertRole(role: RoleGrowPos) {
        try {
            let id = uuid.v4();
            role.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO roles SET ?', role);
            con.release()
            return role
        } catch (error) {            
            this.log.insertLog(LogEnum.ERROR,`${RoleDAOGrowPos.name} -> ${this.insertRole.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getRole() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM roles;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${RoleDAOGrowPos.name} -> ${this.getRole.name}: ${error}`)
            throw new Error(error)
        }
    }
    
    public async getRoleById(roleId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        permissions
                        FROM roles
                        WHERE ID = ?;`, [roleId]);
            if (query.length > 0) {
                query[0]['permissions'] = JSON.parse(query[0]['permissions'].toString());
            }
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${RoleDAOGrowPos.name} -> ${this.getRoleById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateRole(role: RoleGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE roles SET
                        name = ?
                        WHERE id = ?;`,
                [role.name,
                role.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${RoleDAOGrowPos.name} -> ${this.updateRole.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteRole(roleId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM roles 
                        WHERE id = ?;`, [roleId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${RoleDAOGrowPos.name} -> ${this.deleteRole.name}: ${error}`)
            throw new Error(error)
        }
    }
}
