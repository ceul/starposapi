
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { AttributeValueGrowPos } from 'models/growpos/attribute-value';

export class AttributeValueDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAttributeValue(attributeValue: AttributeValueGrowPos,attribute_id: string, con = null) {
        try {
            let ban = true
            attributeValue.id = uuid.v4();
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO attributevalue (id, attribute_id, value) VALUES (?,?,?)', [attributeValue.id,
            attribute_id,
            attributeValue.value]);
            if (!ban) {
                con.release()
            }
            return attributeValue
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeValueDAOGrowPos.name} -> ${this.insertAttributeValue.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeValue() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        attribute_id,
                        value
                        FROM attributeValue;`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeValueDAOGrowPos.name} -> ${this.getAttributeValue.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeValueById(attributeValueId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        value
                        FROM attributevalue
                        WHERE ID = ?;`, [attributeValueId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeValueDAOGrowPos.name} -> ${this.getAttributeValueById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeByAttribute(attributeId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        value
                        FROM attributevalue
                        WHERE attribute_id = ?;`, [attributeId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeValueDAOGrowPos.name} -> ${this.getAttributeValueById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateAttributeValue(attributeValue: AttributeValueGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE attributevalue SET
                        value = ?
                        WHERE id = ?;`,
                [attributeValue.id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeValueDAOGrowPos.name} -> ${this.updateAttributeValue.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttributeValueById(attributeValueId: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM attributevalue 
                        WHERE id = ?;`, [attributeValueId]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeValueDAOGrowPos.name} -> ${this.deleteAttributeValueById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttributeValueByAttribute(attributeId: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM attributevalue 
                        WHERE attribute_id = ?;`, [attributeId] );
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeValueDAOGrowPos.name} -> ${this.deleteAttributeValueById.name}: ${error}`)
            throw new Error(error)
        }
    }

}
