
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { CloseCashGrowPos } from '../../models/growpos/close-cash';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { PaymentGrowPos } from '../../models/growpos/payment';
import * as _ from "lodash"

export class CloseCashDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertCloseCash(closeCash: CloseCashGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let money = uuid.v4();
            closeCash.money = money;
            await con.query('INSERT INTO closedcash SET ?', closeCash);
            if (!ban) {
                con.release()
            }
            return closeCash
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.insertCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCloseCash() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        money,
                        host
                        FROM closedcash;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.getCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCloseCashById(closeCashId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        money,
                        host
                        FROM closedcash
                        WHERE money = ?;`, [closeCashId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.getCloseCashById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getLastCloseCashByHost(host: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT * FROM closedcash 
                    WHERE host=? 
                    and dateend IS NULL`, [host]);
            if(query.length === 0){
                let closeCash = new CloseCashGrowPos()
                closeCash.host = host
                closeCash.nosales = 0
                closeCash.hostsequence = 1
                closeCash.datestart = new Date()
                query = [await this.insertCloseCash(closeCash, con)]
            }
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.getLastCloseCashByHost.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCloseCashByHostAndBySequence(host: string, sequence: number) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT * FROM closedcash 
                    WHERE host = ? 
                    and hostsequence = ?`, [host,sequence]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.getCloseCashByHostAndBySequence.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateCloseCash(closeCash: CloseCashGrowPos) {
        try {
            let con = await this.connection.getConnection()
            await con.query('START TRANSACTION').then(async transaction => {
                let query = await con.query(`UPDATE closedcash SET
                dateend = ?
                WHERE money = ?;`,
                    [closeCash.dateend,
                    closeCash.money]);
                closeCash.datestart = closeCash.dateend
                closeCash.dateend = null
                closeCash.hostsequence = closeCash.hostsequence + 1
                closeCash.nosales = 0
                let returnCloseCash = await this.insertCloseCash(closeCash)
                await con.query('COMMIT')
                await con.release();
                
                return returnCloseCash
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.updateCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }


    public async isCashActive(closeCash: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT money FROM closedcash WHERE dateend IS NULL AND money = ?;`,
                closeCash);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.isCashActive.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async moveCash(closeCash: string, payment: PaymentGrowPos, person: string) {
        try {
            let id = uuid.v4()
            let con = await this.connection.getConnection()
            await con.query('START TRANSACTION').then(async transaction => {
                await con.query(`INSERT INTO receipts (
                ID, 
                MONEY, 
                DATENEW,
                PERSON) 
                VALUES (?, ?, ?, ?);`, [id, closeCash, new Date(), person]);
                await con.query(`INSERT INTO payments (
                ID, 
                RECEIPT, 
                PAYMENT, 
                TOTAL, 
                NOTES) VALUES (?, ?, ?, ?, ?)`, [uuid.v4(), id, payment.payment, payment.total, payment.notes]);
                await con.query('COMMIT')
                await con.release();
                return true
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.moveCash.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCloseCashListByDateReport(dateStart: Date, dateEnd: Date) {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT closedcash.host,
            closedcash.hostsequence,
            closedcash.money,
            closedcash.datestart,
            closedcash.dateend,
            payments.payment,
            SUM(payments.total) AS total
            FROM closedcash, payments, receipts
            WHERE closedcash.money = receipts.money 
            AND payments.receipt = receipts.ID 
            AND closedcash.dateend > ? AND closedcash.dateend < ?
            GROUP BY closedcash.host, closedcash.hostsequence, closedcash.money, closedcash.datestart, closedcash.dateend, payments.payment
            ORDER BY closedcash.host, closedcash.hostsequence;`, [dateStart, dateEnd]);
            if (query.length > 0) {
                result = _.chain(query).groupBy("money").map((money) => {
                    return {
                        host: _.get(_.find(money, 'host'), 'host'),
                        hostsequence: _.get(_.find(money, 'hostsequence'), 'hostsequence'),
                        datestart: _.get(_.find(money, 'datestart'), 'datestart'),
                        dateend: _.get(_.find(money, 'dateend'), 'dateend'),
                        details: _.map(money, (item) => {
                            return {
                                payment: item['payment'],
                                total: item['total'],
                            }
                        })
                    }
                }).value();
            }
            con.release()
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.getCloseCashListByDateReport.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCashFlowReport(datestart: Date, dateend: Date) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
            payments.payment,
            SUM(payments.TOTAL) AS total
            FROM closedcash, payments, receipts
            WHERE closedcash.MONEY = receipts.MONEY
            AND payments.RECEIPT = receipts.ID AND closedcash.dateend >= ? AND closedcash.dateend <= ?
            GROUP BY payments.PAYMENT`, [datestart, dateend]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CloseCashDAOGrowPos.name} -> ${this.getCashFlowReport.name}: ${error}`)
            throw new Error(error)
        }
    }

}
