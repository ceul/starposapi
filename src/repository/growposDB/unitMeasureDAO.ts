
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { UnitOfMeasureGrowPos } from '../../models/growpos/unit-measure';
import { LogDAOGrowPos } from './logDAO';
import { LogEnum } from '../../models/growpos/log.enum';

export class UnitMeasureDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertUnitMeasure(unitMeasure: UnitOfMeasureGrowPos) {
        try {
            let id = uuid.v4();
            unitMeasure.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO uom SET ?', unitMeasure);
            con.release()
            return unitMeasure
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${UnitMeasureDAOGrowPos.name} -> ${this.insertUnitMeasure.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getUnitMeasure() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM uom;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${UnitMeasureDAOGrowPos.name} -> ${this.getUnitMeasure.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getUnitMeasureById(unitMeasureId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM uom
                        WHERE ID = ?;`, [unitMeasureId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${UnitMeasureDAOGrowPos.name} -> ${this.getUnitMeasureById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateUnitMeasure(unitMeasure: UnitOfMeasureGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE uom SET
                        name = ?
                        WHERE id = ?;`,
                [unitMeasure.name,
                unitMeasure.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${UnitMeasureDAOGrowPos.name} -> ${this.updateUnitMeasure.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteUnitMeasure(unitMeasureId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM uom 
                        WHERE id = ?;`, [unitMeasureId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${UnitMeasureDAOGrowPos.name} -> ${this.deleteUnitMeasure.name}: ${error}`)
            throw new Error(error)
        }
    }

}
