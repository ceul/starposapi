
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { SupplierGrowPos } from 'models/growpos/supplier';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class SupplierDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertSupplier(supplier: SupplierGrowPos) {
        try {
            let id = uuid.v4();
            supplier.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO suppliers SET ?', supplier);
            con.release()
            return supplier
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SupplierDAOGrowPos.name} -> ${this.insertSupplier.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getSupplier() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        searchkey,
                        taxid,
                        name,
                        maxdebt,
                        address,
                        address2,
                        postal,
                        city,
                        region,
                        country,
                        firstname,
                        lastname,
                        email,
                        phone,
                        phone2,
                        fax,
                        notes,
                        visible,
                        curdate,
                        curdebt,
                        vatid
                        FROM suppliers;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SupplierDAOGrowPos.name} -> ${this.getSupplier.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getVisibleSupplier() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        searchkey,
                        taxid,
                        name,
                        maxdebt,
                        postal,
                        email,
                        phone,
                        curdebt
                        FROM suppliers WHERE visible = TRUE AND (1=1) ORDER BY name;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SupplierDAOGrowPos.name} -> ${this.getVisibleSupplier.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getSupplierById(supplierId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        searchkey,
                        taxid,
                        name,
                        maxdebt,
                        address,
                        address2,
                        postal,
                        city,
                        region,
                        country,
                        firstname,
                        lastname,
                        email,
                        phone,
                        phone2,
                        fax,
                        notes,
                        visible,
                        curdate,
                        curdebt,
                        vatid
                        FROM suppliers
                        WHERE id = ?;`, [supplierId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SupplierDAOGrowPos.name} -> ${this.getSupplierById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateSupplier(supplier: SupplierGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE suppliers SET
                        searchkey = ?,
                        taxid = ?,
                        name = ?,
                        maxdebt = ?,
                        address = ?,
                        address2 = ?,
                        postal = ?,
                        city = ?,
                        region = ?,
                        country = ?,
                        firstname = ?,
                        lastname = ?,
                        email = ?,
                        phone = ?,
                        phone2 = ?,
                        fax = ?,
                        notes = ?,
                        visible = ?,
                        curdate = ?,
                        curdebt = ?,
                        vatid = ?
                        WHERE id = ?;`,
                [supplier.searchkey,
                supplier.taxid,
                supplier.name,
                supplier.maxdebt,
                supplier.address,
                supplier.address2,
                supplier.postal,
                supplier.city,
                supplier.region,
                supplier.country,
                supplier.firstname,
                supplier.lastname,
                supplier.email,
                supplier.phone,
                supplier.phone2,
                supplier.fax,
                supplier.notes,
                supplier.visible,
                supplier.curdate,
                supplier.curdebt,
                supplier.vatid,
                supplier.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SupplierDAOGrowPos.name} -> ${this.updateSupplier.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteSupplier(supplierId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM suppliers 
                        WHERE id = ?;`, [supplierId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SupplierDAOGrowPos.name} -> ${this.deleteSupplier.name}: ${error}`)
            throw new Error(error)
        }
    }

}
