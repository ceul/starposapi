
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { ProductGrowPos } from 'models/growpos/product';
import { rejects } from 'assert';
import { ProductFilterGrowPos } from '../../models/growpos/product-filter';
import { FilterOperator } from '../../models/growpos/filter-operators.enum';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { ProductCharacteristicDAOGrowPos } from './product_characteristicDAO';
import * as _ from "lodash"

export class ProductDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertProductFunc(product: ProductGrowPos, con = null) {
        try {
            await con.query(`INSERT INTO products ( 
                id,
                reference,
                code,
                codetype,
                name,
                pricebuy,
                pricesell,
                category,
                taxcat,
                attributeset_id,
                stockcost,
                stockvolume,
                iscom,
                isscale,
                isconstant,
                printkb,
                sendstatus,
                isservice,
                attributes,
                display,
                isvprice,
                isverpatrib,
                texttip,
                warranty,
                stockunits,
                printto,
                supplier,
                uom,
                flag,
                description,
                short_description,
                weigth,
                length,
                height,
                width) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
                [product.id,
                product.reference,
                product.code,
                product.codetype,
                product.name,
                product.pricebuy,
                product.pricesell,
                product.category,
                product.taxcat,
                product.attributeset_id,
                product.stockcost,
                product.stockvolume,
                product.iscom,
                product.isscale,
                product.isconstant,
                product.printkb,
                product.sendstatus,
                product.isservice,
                product.attributes,
                product.display,
                product.isvprice,
                product.isverpatrib,
                product.texttip,
                product.warranty,
                product.stockunits,
                product.printto,
                product.supplier,
                product.uom,
                product.flag,
                product.description,
                product.short_description,
                product.weigth,
                product.length,
                product.height,
                product.width])

            if (product.inCat) {
                await con.query('INSERT INTO products_cat SET ?', { product: product.id, catorder: product.catorder })
            }
            
            return product
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.insertProductFunc.name}: ${error}`)
            throw new Error(error)
        }
    }
    public async insertProduct(product: ProductGrowPos, con = null) {
        try {
            let id = uuid.v4();
            product.id = id;
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()

                let trasanction = await con.query('START TRANSACTION').then(async (transaction) => {
                    let product2 = await this.insertProductFunc(product, con)
                    
                    // Save characteristic
                    if (product.productCharacteristic != null && product.productCharacteristic.length > 0 && !!product.productCharacteristic ){
                        let productCharacteristicDAOGrowPos: ProductCharacteristicDAOGrowPos = new ProductCharacteristicDAOGrowPos();
                        for (let index = 0; index < product.productCharacteristic.length; index++) {
                            product.productCharacteristic[index].id_product = product2.id;
                            productCharacteristicDAOGrowPos.insertProductCharacteristic(product.productCharacteristic[index], con);
                        }
                    }
                    await con.query('COMMIT')
                    con.release()
                    
                    return product2
                }).catch(err => {
                    con.rollback(() => {
                        con.release();
                    });
                    throw new Error(err)
                })
                return trasanction
            } else {
                let product2 = await this.insertProductFunc(product, con)
                return product2
            }

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.insertProduct.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getConstCatProduct() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
                        products.id,
                        products.reference,
                        products.code,
                        products.codetype,
                        products.name,
                        products.pricebuy,
                        products.pricesell,
                        products.category,
                        products.taxcat,
                        products.attributeset_id,
                        products.stockcost,
                        products.stockvolume,
                        products.iscom,
                        products.isscale,
                        products.isconstant,
                        products.printkb,
                        products.sendstatus,
                        products.isservice,
                        products.attributes,
                        products.display,
                        products.isvprice,
                        products.isverpatrib,
                        products.texttip,
                        products.warranty,
                        products.stockunits,
                        products.printto,
                        products.supplier,
                        products.uom,
                        products.flag,
                        products.description,
                        products.short_description,
                        products.weigth,
                        products.length,
                        products.height,
                        products.width
                        FROM categories INNER JOIN products 
                        ON (products.category = categories.id) 
                        WHERE products.isconstant = TRUE 
                        ORDER BY categories.name,
                        products.name`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getConstCatProduct.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductList() {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT products.id,
                        products.reference AS reference,
                        products.code AS code,
                        products.name AS name,
                        products.pricebuy AS pricebuy,
                        products.pricesell AS pricesell,
                        TC.id AS taxcat,
                        TC.name AS taxcatname,
                        taxes.rate AS tax,
                        categories.id AS category,
                        categories.name AS categoryname,
                        SUM(stockcurrent.units) AS units FROM (((products products INNER JOIN stockcurrent stockcurrent ON 
                            (products.id = stockcurrent.PRODUCT)) LEFT OUTER JOIN categories categories ON 
                            (products.category = categories.id)) LEFT OUTER JOIN taxcategories TC ON 
                            (products.taxcat = TC.id)) LEFT OUTER JOIN taxes taxes ON (products.taxcat = taxes.category) WHERE (1=1) GROUP BY id  ORDER BY categoryname ASC,
                        products.name ASC;`)
            if (query.length > 0) {
                result = _.chain(query).groupBy("category").map((category) => {
                    return {
                        name: _.get(_.find(category, 'categoryname'), 'categoryname'),
                        details: _.map(category, (item) => {
                            return {
                                reference: item['reference'],
                                name: item['name'],
                                units: item['units'],
                                pricebuy: item['pricebuy'],
                                buyvalue: item['buyvalue'],
                                pricesell: item['pricesell'],
                                pricegross: item['tax'] !== 0 ? item['pricesell'] * item['tax'] : item['pricesell'],
                            }
                        })
                    }
                }).value();
            }
            con.release()
            return result

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductsByCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductsByCategory(categoryId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
                    P.id,
                    P.reference,
                    P.code,
                    P.codetype,
                    P.name,
                    P.pricebuy,
                    P.pricesell,
                    P.category,
                    P.taxcat,
                    P.attributeset_id,
                    P.stockcost,
                    P.stockvolume,
                    P.iscom,
                    P.isscale,
                    P.isconstant,
                    P.printkb,
                    P.sendstatus,
                    P.isservice,
                    P.attributes,
                    P.display,
                    P.isvprice,
                    P.isverpatrib,
                    P.texttip,
                    P.warranty,
                    P.stockunits,
                    P.printto,
                    P.supplier,
                    P.uom,
                    P.flag,
                    P.description,
                    P.short_description,
                    P.weigth,
                    P.length,
                    P.height,
                    P.width
                    FROM products P,
                    products_cat O WHERE 
                    P.id = O.product AND P.category = ?
                    ORDER BY O.catorder,
                    P.name`, [categoryId])
            con.release()
            return query

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductsByCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProduct() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    P.id,
                    P.reference,
                    P.code,
                    P.codetype,
                    P.name,
                    P.pricebuy,
                    P.pricesell,
                    P.category,
                    P.taxcat,
                    P.attributeset_id,
                    P.stockcost,
                    P.stockvolume,
                    P.iscom,
                    P.isscale,
                    P.isconstant,
                    P.printkb,
                    P.sendstatus,
                    P.isservice,
                    P.attributes,
                    P.display,
                    P.isvprice,
                    P.isverpatrib,
                    P.texttip,
                    P.warranty,
                    P.stockunits,
                    P.printto,
                    P.supplier,
                    P.uom,
                    P.flag,
                    P.description,
                    P.short_description,
                    P.weigth,
                    P.length,
                    P.height,
                    P.width,
                    CASE WHEN C.product IS NULL THEN FALSE ELSE TRUE END AS inCat,
                    C.catorder
                    FROM products P LEFT OUTER JOIN products_cat C ON P.id = C.product WHERE (1=1) ORDER BY P.reference;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProduct.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductById(productId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id,
                    reference,
                    code,
                    codetype,
                    name,
                    pricebuy,
                    pricesell,
                    category,
                    taxcat,
                    attributeset_id,
                    stockcost,
                    stockvolume,
                    iscom,
                    isscale,
                    isconstant,
                    printkb,
                    sendstatus,
                    isservice,
                    attributes,
                    display,
                    isvprice,
                    isverpatrib,
                    texttip,
                    warranty,
                    stockunits,
                    printto,
                    supplier,
                    uom,
                    flag,
                    description,
                    short_description,
                    weigth,
                    length,
                    height,
                    width
                    FROM products
                    WHERE ID = ?;`, [productId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTotalByCloseCash(closedcash: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT products.name, 
            SUM(ticketlines.UNITS) as units, 
            ticketlines.PRICE as price, 
            taxes.RATE as rate 
            FROM ticketlines, 
            tickets, 
            receipts, 
            products, 
            taxes 
            WHERE ticketlines.PRODUCT = products.ID AND ticketlines.TICKET = tickets.ID 
            AND tickets.ID = receipts.ID AND ticketlines.TAXID = taxes.ID AND 
            receipts.MONEY = ?
            GROUP BY products.name, ticketlines.PRICE, taxes.RATE
            `, [closedcash]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getTotalByCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }
    public async getProductByBarCode(productId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id,
                    reference,
                    code,
                    codetype,
                    name,
                    pricebuy,
                    pricesell,
                    category,
                    taxcat,
                    attributeset_id,
                    stockcost,
                    stockvolume,
                    iscom,
                    isscale,
                    isconstant,
                    printkb,
                    sendstatus,
                    isservice,
                    attributes,
                    display,
                    isvprice,
                    isverpatrib,
                    texttip,
                    warranty,
                    stockunits,
                    printto,
                    supplier,
                    uom,
                    flag,
                    description,
                    short_description,
                    weigth,
                    length,
                    height,
                    width
                    FROM products
                    WHERE code = ?;`, [productId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductByBarCode.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateProductFunc(product: ProductGrowPos, con = null) {
        try {

            await con.query(`UPDATE products SET
                        reference = ?,
                        code = ?,
                        codetype = ?,
                        name = ?,
                        pricebuy = ?,
                        pricesell = ?,
                        category = ?,
                        taxcat = ?,
                        attributeset_id = ?,
                        stockcost = ?,
                        stockvolume = ?,
                        iscom = ?,
                        isscale = ?,
                        isconstant = ?,
                        printkb = ?,
                        sendstatus = ?,
                        isservice = ?,
                        attributes = ?,
                        display = ?,
                        isvprice = ?,
                        isverpatrib = ?,
                        texttip = ?,
                        warranty = ?,
                        stockunits = ?,
                        printto = ?,
                        supplier = ?,
                        uom = ?,
                        flag = ?,
                        description = ?,
                        short_description = ?,
                        weigth = ?,
                        length = ?,
                        height = ?,
                        width = ?
                    WHERE id = ?;`,
                [product.reference,
                product.code,
                product.codetype,
                product.name,
                product.pricebuy,
                product.pricesell,
                product.category,
                product.taxcat,
                product.attributeset_id,
                product.stockcost,
                product.stockvolume,
                product.iscom,
                product.isscale,
                product.isconstant,
                product.printkb,
                product.sendstatus,
                product.isservice,
                product.attributes,
                product.display,
                product.isvprice,
                product.isverpatrib,
                product.texttip,
                product.warranty,
                product.stockunits,
                product.printto,
                product.supplier,
                product.uom,
                product.flag,
                product.description,
                product.short_description,
                product.weigth,
                product.length,
                product.height,
                product.width,
                product.id])
            if (product.inCat) {
                let query = await con.query(`UPDATE products_cat SET CATORDER = ? WHERE PRODUCT = ?`, [product.catorder, product.id])
                if (query.affectedRows === 0) {
                    await con.query('INSERT INTO products_cat (PRODUCT, CATORDER) VALUES (?, ?)', [product.id, product.catorder])
                }
            } else {
                await con.query(`DELETE FROM products_cat WHERE PRODUCT = ?`, [product.id])
            }
            return true
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.updateProductFunc.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductImage(productId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    image
                    FROM products
                    WHERE id = ?;`, [productId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductImage.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateProduct(product: ProductGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
                await con.query('START TRANSACTION').then(async transaction => {
                    await this.updateProductFunc(product, con)
                    await con.query('COMMIT')
                    con.release();
                    
                    return true
                }).catch(error => {
                    con.rollback(() => {
                        con.release();
                    });
                    throw new Error(error)
                });
            } else {
                let product2 = await this.updateProductFunc(product, con)
                return product2
            }
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.updateProduct.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async uploadPicture(id: string, img: Buffer) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE products SET
                        image = ?
                    WHERE id = ?;`,
                [img, id])
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.uploadPicture.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteProductFunc(productId: string, con) {
        try {
            await con.query(`DELETE FROM products_cat 
                            WHERE product = ?;`, [productId])
            await con.query(`DELETE FROM products 
                                    WHERE id = ?;`, [productId])
            return true
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.deleteProductFunc.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteProduct(productId: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
                await con.query('START TRANSACTION').then(async (transaction) => {
                    await this.deleteProductFunc(productId, con)
                    await con.query('COMMIT')
                    con.release();
                    
                    return true

                }).catch(error => {
                    con.rollback(() => {
                        con.release();
                    });
                    throw new Error(error)
                });
            } else {
                let product = await this.deleteProductFunc(productId, con)
                return product
            }
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.deleteProduct.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteProductByCategoryFunc(productCategory: string, con) {
        try {
            await con.query(`DELETE products_cat FROM products_cat INNER JOIN 
            products ON products_cat.product = products.id WHERE 
            products.category=?;`, [productCategory])

            await con.query(`DELETE FROM products 
                                WHERE category = ?;`, [productCategory])
            return true
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.deleteProductByCategoryFunc.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteProductByCategory(productCategory: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()

                await con.query('START TRANSACTION').then(async (transaction) => {
                    await this.deleteProductByCategoryFunc(productCategory, con)
                    await con.query('COMMIT')
                    if (!ban) {
                        con.release()
                    }
                    
                    return true

                }).catch(error => {
                    con.rollback(() => {
                        con.release();
                    });
                    throw new Error(error)
                });
            } else {
                let product2 = await this.deleteProductByCategoryFunc(productCategory, con)
                return product2
            }

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.deleteProductByCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductFiltered(filter: ProductFilterGrowPos) {
        try {
            let params = []
            let sql = `SELECT
            id,
            reference,
            code,
            codetype,
            name,
            pricebuy,
            pricesell,
            category,
            taxcat,
            attributeset_id,
            stockcost,
            stockvolume,
            iscom,
            isscale,
            isconstant,
            printkb,
            sendstatus,
            isservice,
            attributes,
            display,
            isvprice,
            isverpatrib,
            texttip,
            warranty,
            stockunits,
            printto,
            supplier,
            uom,
            flag,
            description,
            short_description,
            weigth,
            length,
            height,
            width
            FROM products WHERE (1=1`
            if (filter.barcode !== undefined) {
                if (filter.barcode !== '') {
                    sql += ' and barcode = ?'
                    params.push(filter.barcode)
                }
            }
            if (filter.category !== undefined) {
                if (filter.category !== '') {
                    sql += ' and category = ?'
                    params.push(filter.category)
                }
            }
            if (filter.name !== undefined) {
                if (filter.name.filter !== FilterOperator.None && filter.name.value !== '') {
                    let op = this.getOperator(filter.name.filter)
                    sql += ` and name ${op} ?`
                    params.push(filter.name.value)
                }
            }
            if (filter.pricebuy !== undefined) {
                if (filter.pricebuy.filter !== FilterOperator.None && filter.pricebuy.value !== null) {
                    let op = this.getOperator(filter.pricebuy.filter)
                    sql += ` and pricebuy ${op} ?`
                    params.push(filter.pricebuy.value)
                }
            }
            if (filter.pricesell !== undefined) {
                if (filter.pricesell.filter !== FilterOperator.None && filter.pricesell.value !== null) {
                    let op = this.getOperator(filter.pricesell.filter)
                    sql += ` and pricesell ${op} ?`
                    params.push(filter.pricesell.value)
                }
            }
            sql += ') ORDER BY reference'
            let con = await this.connection.getConnection()
            let query = await con.query(sql, params)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductFiltered.name}: ${error}`)
            throw new Error(error)
        }
    }

    public getOperator(operator: number) {
        try {
            if (operator === FilterOperator.None) {
                return
            } else if (operator === FilterOperator.Equals) {
                return '='
            } else if (operator === FilterOperator.Distinct) {
                return '<>'
            } else if (operator === FilterOperator.Greater) {
                return '>'
            } else if (operator === FilterOperator.Less) {
                return '<'
            } else if (operator === FilterOperator.GreaterOrEqual) {
                return '>='
            } else if (operator === FilterOperator.LessOrEqual) {
                return '<='
            }
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getOperator.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductBestSelling() {
        try {
            let con = await this.connection.getConnection()
            let date = new Date()
            let query = await con.query(`SELECT
                        products.name as name, 
                        SUM(ticketlines.UNITS) as units, 
                        ticketlines.PRICE as price, 
                        taxes.RATE as rate
                        FROM ticketlines, tickets, receipts, products, taxes 
                        WHERE ticketlines.PRODUCT = products.ID 
                        AND ticketlines.TICKET = tickets.ID 
                        AND tickets.ID = receipts.ID 
                        AND ticketlines.TAXID = taxes.ID 
                        AND receipts.datenew >= ?
                        GROUP BY products.name, ticketlines.PRICE, taxes.RATE 
                        ORDER BY units DESC LIMIT 1`, [`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductBestSelling.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getWithLowerInventory() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
                    products.name as name, 
                    stockcurrent.units AS current, 
                    stocklevel.stocksecurity AS minimum, 
                    stocklevel.stockmaximum AS maximum 
                    FROM ((products 
                        INNER JOIN stockcurrent 
                        ON products.id = stockcurrent.product) 
                        LEFT JOIN stocklevel ON products.id = stocklevel.product) 
                    ORDER BY Current ASC LIMIT 1`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getWithLowerInventory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductSalesReport(datestart: Date, dateend: Date) {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
            closedcash.host,
            closedcash.money,
            closedcash.dateend,
            products.reference,
            products.name,
            pricesell AS price,
            Sum(ticketlines.units) AS units,
            Sum((products.pricesell+products.pricesell*taxes.rate)*units) AS value,
            Sum((products.pricesell+products.pricesell*taxes.rate)*ticketlines.units-(ticketlines.price+(ticketlines.price*taxes.rate))*ticketlines.units) AS discount,
            Sum((ticketlines.price+(ticketlines.price*taxes.rate))*ticketlines.UNITS) AS total
            FROM ((tickets INNER JOIN (ticketlines INNER JOIN products ON ticketlines.product = products.id)
            ON tickets.id = ticketlines.TICKET) INNER JOIN (receipts INNER JOIN closedcash
            ON receipts.money = closedcash.money) ON tickets.id = receipts.id) INNER JOIN taxes ON ticketlines.TAXid = taxes.id
            WHERE closedcash.money = receipts.money AND receipts.id = tickets.id AND tickets.id = ticketlines.TICKET 
            AND closedcash.dateend >= ? AND closedcash.dateend < ?
            GROUP BY closedcash.host, closedcash.money, closedcash.dateend, products.reference, products.name
            ORDER BY products.name, closedcash.host, closedcash.dateend`, [datestart, dateend]);
            if (query.length > 0) {
                result = _.chain(query).groupBy("reference").map((reference) => {
                    let units = 0
                    let sales = 0
                    let discount = 0
                    let total = 0
                    return {
                        name: _.get(_.find(reference, 'name'), 'name'),
                        hostsequence: _.get(_.find(reference, 'hostsequence'), 'hostsequence'),
                        datestart: _.get(_.find(reference, 'datestart'), 'datestart'),
                        dateend: _.get(_.find(reference, 'dateend'), 'dateend'),
                        details: _.map(reference, (item) => {

                            units = units + item['units']
                            sales = sales + item['value']
                            discount = discount + item['discount']
                            total = total + item['total']
                            return {
                                host: item['host'],
                                dateend: item['dateend'],
                                units: item['units'],
                                price: item['price'],
                                sales: item['value'],
                                discount: item['discount'],
                                total: item['total'],
                            }
                        }),
                        units,
                        sales,
                        discount,
                        total,
                    }
                }).value();
            }
            con.release();
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProductSalesReport.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProfitSales(dateStart: Date, dateEnd: Date) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT products.reference,
            products.name,
            products.pricebuy,
            products.pricesell,
            SUM(ticketlines.units) AS sold_units,
            SUM(ticketlines.units * products.pricebuy) AS cost_value,
            SUM(ticketlines.units * products.pricesell) AS expected_sales_value,
            SUM(ticketlines.PRICE) AS actual_sales_value,
            SUM(ticketlines.units * products.pricesell) - SUM(ticketlines.units * products.pricebuy) AS expected_profit,
            SUM(ticketlines.price * ticketlines.units) - SUM(ticketlines.units * products.pricebuy) AS actual_profit
            FROM (ticketlines ticketlines INNER JOIN receipts receipts
            ON (ticketlines.TICKET = receipts.ID))
            LEFT OUTER JOIN products products
            ON (ticketlines.product = products.ID)
            WHERE receipts.datenew >= ? AND receipts.datenew < ?
            GROUP BY ticketlines.product
            ORDER BY products.reference ASC`, [dateStart, dateEnd]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductDAOGrowPos.name} -> ${this.getProfitSales.name}: ${error}`)
            throw new Error(error)
        }
    }
}
