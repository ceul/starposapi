
import { DataBaseService } from '../../dataBaseService';
import { LogDAOGrowPos } from './logDAO';
import { LogEnum } from '../../models/growpos/log.enum';

export class PaymentDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async getTotalPaymentsByCloseCash(closeCash: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT COUNT(*) as transactions, 
            SUM(payments.TOTAL) as money FROM payments, receipts 
            WHERE payments.RECEIPT = receipts.ID 
            AND receipts.MONEY = ?`, [closeCash]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${PaymentDAOGrowPos.name} -> ${this.getTotalPaymentsByCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getByCloseCash(closeCash: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT payments.PAYMENT as name, 
            SUM(payments.TOTAL) as total, 
            payments.NOTES as notes FROM payments, receipts 
            WHERE payments.RECEIPT = receipts.ID AND receipts.MONEY = ? 
            GROUP BY payments.PAYMENT, payments.NOTES`, [closeCash]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${PaymentDAOGrowPos.name} -> ${this.getByCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTransactionsReport(datestart: Date, dateend: Date) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
            tickets.TICKETID AS ticket_no,
            receipts.DATENEW AS date,
            people.NAME AS person,
            payments.PAYMENT AS payment,
            payments.notes,
            payments.TOTAL AS value,
            payments.tendered
            FROM((tickets tickets 
            LEFT OUTER JOIN people people ON(tickets.person = people.ID))
            RIGHT OUTER JOIN receipts receipts ON(receipts.ID = tickets.ID))
            LEFT OUTER JOIN payments payments ON(receipts.ID = payments.RECEIPT)
            WHERE receipts.datenew >= ? AND receipts.datenew <= ?
            ORDER BY date ASC`, [datestart, dateend]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${PaymentDAOGrowPos.name} -> ${this.getTransactionsReport.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTransactionsReportByPayment(datestart: Date, dateend: Date) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT COUNT(tickets.TICKETID) AS ticket_no,
            people.NAME AS person,
            payments.PAYMENT AS payment,
            SUM(payments.TOTAL) AS total,
            SUM(payments.tendered) as tendered
            FROM((tickets tickets 
            LEFT OUTER JOIN people people ON(tickets.person = people.ID))
            RIGHT OUTER JOIN receipts receipts ON(receipts.ID = tickets.ID))
            LEFT OUTER JOIN payments payments ON(receipts.ID = payments.RECEIPT)
            WHERE receipts.datenew >= ? AND receipts.datenew <= ?
            GROUP BY payment
            ORDER BY payment ASC`, [datestart, dateend]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${PaymentDAOGrowPos.name} -> ${this.getTransactionsReport.name}: ${error}`)
            throw new Error(error)
        }
    }

}
