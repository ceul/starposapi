//    StarPos
//    Copyright (c) 2020 GrowPos
//    http://starpos.co
//
//    This file is part of StarPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { PeopleRolesGrowPos } from '../../models/growpos/people-roles';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class PeopleRolesDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertPeopleRole(role: PeopleRolesGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO people_roles SET ?', role);
            con.release()
            return role
        } catch (error) {            
            this.log.insertLog(LogEnum.ERROR,`${PeopleRolesDAOGrowPos.name} -> ${this.insertPeopleRole.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getPeopleRole() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id_people,
                        id_rol
                        FROM people_roles;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PeopleRolesDAOGrowPos.name} -> ${this.getPeopleRole.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getPeopleRoleById(peopleId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id_people,
                        id_rol
                        FROM people_roles
                        WHERE id_people = ?;`, [peopleId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PeopleRolesDAOGrowPos.name} -> ${this.getPeopleRoleById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deletePeopleRole(peopleId: string, roleId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM people_roles 
                        WHERE id_people = ? AND id_rol = ?;`, [peopleId, roleId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PeopleRolesDAOGrowPos.name} -> ${this.deletePeopleRole.name}: ${error}`)
            throw new Error(error)
        }
    }
}
