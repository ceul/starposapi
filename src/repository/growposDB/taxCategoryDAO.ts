
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { TaxCategoryGrowPos } from 'models/growpos/tax-category';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class TaxCategoryDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertTaxCategory(taxCategory: TaxCategoryGrowPos) {
        try {
            let id = uuid.v4();
            taxCategory.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO taxcategories SET ?', taxCategory);
            con.release()
            return taxCategory
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxCategoryDAOGrowPos.name} -> ${this.insertTaxCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTaxCategory() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM taxcategories;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxCategoryDAOGrowPos.name} -> ${this.getTaxCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTaxCategoryById(taxCategoryId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM taxcategories
                        WHERE ID = ?;`, [taxCategoryId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxCategoryDAOGrowPos.name} -> ${this.getTaxCategoryById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateTaxCategory(taxCategory: TaxCategoryGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE taxcategories SET
                        name = ?
                        WHERE id = ?;`,
                [taxCategory.name,
                taxCategory.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxCategoryDAOGrowPos.name} -> ${this.updateTaxCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteTaxCategory(taxCategoryId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM taxcategories 
                        WHERE id = ?;`, [taxCategoryId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxCategoryDAOGrowPos.name} -> ${this.deleteTaxCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTotalTaxesByCloseCash(closeCash: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT taxcategories.NAME as name, 
            SUM(taxlines.AMOUNT) as amount, 
            SUM(taxlines.BASE) as base, 
            SUM(taxlines.BASE + taxlines.AMOUNT) as gross 
            FROM receipts, taxlines, taxes, taxcategories 
            WHERE receipts.ID = taxlines.RECEIPT AND taxlines.TAXID = taxes.ID 
            AND taxes.CATEGORY = taxcategories.ID 
            AND receipts.MONEY = ? GROUP BY taxcategories.NAME;`, [closeCash]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TaxCategoryDAOGrowPos.name} -> ${this.getTotalTaxesByCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }

}
