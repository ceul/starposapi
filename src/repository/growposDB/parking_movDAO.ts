//    growPos
//    Copyright (c) 2020 growPos
//    http://growpos.co
//
//    This file is part of growPos
//
//    GrowPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { LogDAOGrowPos } from './logDAO';
import { LogEnum } from '../../models/growpos/log.enum';
import { ParkinMovGrowPos } from '../../models/growpos/parking_mov';

export class ParkinMovDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertParkingMov(parking_mov: ParkinMovGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`INSERT INTO parking_mov (
                places,
                plate,
                receipt,
                date_arrival,
                date_departure,
                type,
                fee,
                note,
                state) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
                    parking_mov.places, parking_mov.plate,
                    parking_mov.receipt,
                    parking_mov.date_arrival,
                    parking_mov.date_departure,
                    parking_mov.type,
                    parking_mov.fee,
                    parking_mov.note,
                    parking_mov.state]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkinMovDAOGrowPos.name} -> ${this.insertParkingMov.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getParkingMov() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        places,
                        plate,
                        receipt,
                        date_arrival,
                        date_departure,
                        type,
                        fee,
                        note,
                        state
                        FROM parking_mov
                        WHERE state = 0;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkinMovDAOGrowPos.name} -> ${this.getParkingMov.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getParkingMovById(parkinMovId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        places,
                        plate,
                        receipt,
                        date_arrival,
                        date_departure,
                        type,
                        fee,
                        note,
                        state
                        FROM parking_mov
                        WHERE plate = ? and state = 0;`, [parkinMovId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkinMovDAOGrowPos.name} -> ${this.getParkingMovById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateParkingMov(obj: ParkinMovGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE parking_mov SET
                        state = ?,
                        date_departure = ?,
                        receipt = ?,
                        type = ?,
                        fee = ?,
                        note = ?
                        WHERE id = ?;`, [obj.state, obj.date_departure, obj.receipt, obj.type, obj.fee, obj.note, obj.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkinMovDAOGrowPos.name} -> ${this.updateParkingMov.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteParkingMov(parkinMovId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM parking_mov
                        WHERE id = ?;`, [parkinMovId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkinMovDAOGrowPos.name} -> ${this.deleteParkingMov.name}: ${error}`)
            throw new Error(error)
        }
    }
}