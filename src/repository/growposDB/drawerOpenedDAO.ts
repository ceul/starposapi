
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import * as _ from "lodash"
import { DrawerOpenedGrowPos } from 'models/growpos/drawer-opened';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class DrawerOpenedDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertDrawerOpened(drawerOpened: DrawerOpenedGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO draweropened SET ?', drawerOpened);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${DrawerOpenedDAOGrowPos.name} -> ${this.insertDrawerOpened.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getDrawerOpened() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        opendate,
                        name,
                        ticketid
                        FROM draweropened;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${DrawerOpenedDAOGrowPos.name} -> ${this.getDrawerOpened.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getDrawerOpenedByUser(userId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        opendate,
                        name,
                        ticketid
                        FROM draweropened
                        WHERE name = ?;`, [userId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${DrawerOpenedDAOGrowPos.name} -> ${this.getDrawerOpenedByUser.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getDrawerOpenedByTicket(productId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        opendate,
                        name,
                        ticketid
                        FROM draweropened
                        WHERE ticketid = ?;`, [productId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${DrawerOpenedDAOGrowPos.name} -> ${this.getDrawerOpenedByTicket.name}: ${error}`)
            throw new Error(error)
        }
    }
}
