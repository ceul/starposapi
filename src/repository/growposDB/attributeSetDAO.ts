
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { AttributeSetGrowPos } from '../../models/growpos/attribute-set';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { AttributeUseDAOGrowPos } from './attributeUseDAO';
import AttributeUseGrowPos from '../../models/growpos/attribute-use';
import { AttributeDAOGrowPos } from './attributeDAO';

export class AttributeSetDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAttributeSet(attributeSet: AttributeSetGrowPos) {
        try {
            let id = uuid.v4();
            attributeSet.id = id;
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                let query = await con.query('INSERT INTO attributeset (id, name) VALUES (?,?)', [attributeSet.id, attributeSet.name]);
                let attributeUseDAO = new AttributeUseDAOGrowPos()
                let attributeuse: AttributeUseGrowPos
                attributeSet.attributes.forEach(async (attribute, index) => {
                    attributeuse = new AttributeUseGrowPos()
                    attributeuse.attributeset_id = attributeSet.id
                    attributeuse.id = uuid.v4();
                    attributeuse.attribute = attribute;
                    attributeuse.line = index;
                    await attributeUseDAO.insertAttributeUse(attributeuse, con)
                })
                await con.query('COMMIT')
                await con.release();
                
                return attributeSet
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeSetDAOGrowPos.name} -> ${this.insertAttributeSet.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeSet(con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM attributeset;`);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeSetDAOGrowPos.name} -> ${this.getAttributeSet.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeSetById(attributeSetId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM attributeset
                        WHERE ID = ?;`, { attributeSetId });
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeSetDAOGrowPos.name} -> ${this.getAttributeSetById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateAttributeSet(attributeSet: AttributeSetGrowPos) {
        try {
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                let query = await con.query(`UPDATE attributeset SET
                        name = ?
                        WHERE id = ?;`,
                    [attributeSet.name,
                    attributeSet.id]);
                let attributeUseDAO = new AttributeUseDAOGrowPos()
                let attributeDAO = new AttributeDAOGrowPos()
                let attributeuse: AttributeUseGrowPos
                let attributes = await attributeDAO.getAttributeByAttributeSet(attributeSet.id)
                attributeSet.attributes.forEach(async (attribute,index) => {
                    let arrIndex = attributes.findIndex(item => item.id === attribute.id)
                    if (arrIndex !== -1) {
                        attributes.splice(arrIndex,1)
                    } else {
                        attributeuse = new AttributeUseGrowPos()
                        attributeuse.attributeset_id = attributeSet.id
                        attributeuse.attribute = attribute;
                        attributeuse.line = index;
                        await attributeUseDAO.insertAttributeUse(attributeuse, con)
                    }

                })
                attributes.forEach(async (element) => {
                    await attributeUseDAO.deleteAttributeUseByAttributeAndAttributeSet(element.id,attributeSet.id, con)
                });
                await con.query('COMMIT')
                await con.release();
                
                return [attributeSet.id]
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeSetDAOGrowPos.name} -> ${this.updateAttributeSet.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttributeSet(attributeSetId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM attributeset 
                        WHERE id = ?;`, [attributeSetId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeSetDAOGrowPos.name} -> ${this.deleteAttributeSet.name}: ${error}`)
            throw new Error(error)
        }
    }

}
