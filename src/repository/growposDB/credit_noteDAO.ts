
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { CreditNoteGrowPos } from '../../models/growpos/credit_note';

export class CreditNoteDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertCreditNote(credit_note: CreditNoteGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO credit_note (receipt, date, note) VALUES (?, ?, ?)', [credit_note.receipt, credit_note.date, credit_note.note]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CreditNoteDAOGrowPos.name} -> ${this.insertCreditNote.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCreditNote() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        receipt,
                        date,
                        note,
                        state
                        FROM credit_note;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CreditNoteDAOGrowPos.name} -> ${this.getCreditNote.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCreditNoteById(creditNoteId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        receipt,
                        date,
                        note,
                        state
                        FROM credit_note
                        WHERE id = ?;`, [creditNoteId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CreditNoteDAOGrowPos.name} -> ${this.getCreditNoteById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateCreditNote(credit_note: CreditNoteGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE credit_note SET
                        state = ?
                        WHERE id = ?;`,
                [credit_note.state,
                credit_note.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CreditNoteDAOGrowPos.name} -> ${this.updateCreditNote.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteCreditNote(creditNoteId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM credit_note
                        WHERE id = ?;`, [creditNoteId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CreditNoteDAOGrowPos.name} -> ${this.deleteCreditNote.name}: ${error}`)
            throw new Error(error)
        }
    }

}
