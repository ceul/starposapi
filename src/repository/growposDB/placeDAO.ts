
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { PlaceGrowPos } from 'models/growpos/place';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class PlaceDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertPlace(place: PlaceGrowPos) {
        try {
            let id = uuid.v4();
            place.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO places SET ?', place);
            con.release()
            return place
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PlaceDAOGrowPos.name} -> ${this.insertPlace.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getPlace(type: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        seats,
                        x,
                        y,
                        floor,
                        customer,
                        waiter,
                        ticketid,
                        tablemoved,
                        width,
                        height,
                        guests,
                        occupied,
                        type
                        FROM places WHERE type=? ORDER BY x;`, [type]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PlaceDAOGrowPos.name} -> ${this.getPlace.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getPlaceById(PlaceId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        seats,
                        x,
                        y,
                        floor,
                        customer,
                        waiter,
                        ticketid,
                        tablemoved,
                        width,
                        height,
                        guests,
                        occupied,
                        type
                        FROM places
                        WHERE ID = ?;`, [PlaceId]);
            /*if (query.length > 0 && query[0].occupied !== null) {
                query[0].occupied = query[0].occupied.toJSON()
            }*/

            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PlaceDAOGrowPos.name} -> ${this.getPlaceById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updatePlace(place: PlaceGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE places SET
                        name = ?,
                        seats = ?,
                        x = ?,
                        y = ?,
                        floor = ?,
                        customer = ?,
                        waiter = ?,
                        ticketid = ?,
                        tablemoved = ?,
                        width = ?,
                        height = ?,
                        guests = ?,
                        occupied = ?,
                        type = ?
                        WHERE id = ?;`,
                [place.name,
                place.seats,
                place.x,
                place.y,
                place.floor,
                place.customer,
                place.waiter,
                place.ticketid,
                place.tablemoved,
                place.width,
                place.height,
                place.guests,
                place.occupied,
                place.type,
                place.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PlaceDAOGrowPos.name} -> ${this.updatePlace.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deletePlace(placeId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM places
                        WHERE id = ?;`, [placeId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${PlaceDAOGrowPos.name} -> ${this.deletePlace.name}: ${error}`)
            throw new Error(error)
        }
    }

}
