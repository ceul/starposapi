//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { AttributesAccountsGrowAccounting } from '../../models/growAccounting/_attributes_accounts';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class AttributesAccountsDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAttributesAccount(data: AttributesAccountsGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _attributes_accounts SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesAccountsDAOGrowAccounting.name} -> ${this.insertAttributesAccount.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributesAccounts() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_account,
                    id_attribute
                    FROM _attributes_accounts;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesAccountsDAOGrowAccounting.name} -> ${this.getAttributesAccounts.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributesAccountsById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_account,
                    id_attribute
                    FROM _attributes_accounts
                    WHERE id_account = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesAccountsDAOGrowAccounting.name} -> ${this.getAttributesAccountsById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttributesAccount(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _attributes_accounts 
                    WHERE id_account = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesAccountsDAOGrowAccounting.name} -> ${this.deleteAttributesAccount.name}: ${error}`)
            throw new Error(error)
        }
    }
}