//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { NiffConceptsGrowAccounting } from '../../models/growAccounting/_niif_concepts';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class niifConceptsDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertNiffConcept(data: NiffConceptsGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _niif_concepts SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${niifConceptsDAOGrowAccounting.name} -> ${this.insertNiffConcept.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getNiffConcepts() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_niif_concept,
                    id_father,
                    description,
                    state
                    FROM _niif_concepts;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${niifConceptsDAOGrowAccounting.name} -> ${this.getNiffConcepts.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getNiffConceptssById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_niif_concept,
                    id_father,
                    description,
                    state
                    FROM _niif_concepts
                    WHERE id_niif_concept = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${niifConceptsDAOGrowAccounting.name} -> ${this.getNiffConceptssById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateNiffConcept(data: NiffConceptsGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _niif_concepts SET
                        id_niif_concept = ? ,
                        id_father = ? ,
                        description = ?,
                        state = ?
                        WHERE id_niif_concept = ?;`,
                    [data.id_niif_concept,
                    data.id_father,
                    data.description,
                    data.state,
                    data.id_niif_concept]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${niifConceptsDAOGrowAccounting.name} -> ${this.updateNiffConcept.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteNiffConcept(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _niif_concepts 
                    WHERE id_niif_concept = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${niifConceptsDAOGrowAccounting.name} -> ${this.deleteNiffConcept.name}: ${error}`)
            throw new Error(error)
        }
    }
}