//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { MovementsGrowAccounting } from '../../models/growAccounting/_movements';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class MovementsDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertMovement(data: MovementsGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _movements SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsDAOGrowAccounting.name} -> ${this.insertMovement.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAMovements() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_movement,
                    id_third_party,
                    date,
                    description,
                    state
                    FROM _movements;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsDAOGrowAccounting.name} -> ${this.getAMovements.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getMovementsById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_movement,
                    id_third_party,
                    date,
                    description,
                    state
                    FROM _movements
                    WHERE id_movement = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsDAOGrowAccounting.name} -> ${this.getMovementsById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async inactiveMovement(data: MovementsGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _movements SET
                        state = 'i'
                        WHERE id_movement = ?;`,
                    [data.id_movement]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsDAOGrowAccounting.name} -> ${this.inactiveMovement.name}: ${error}`)
            throw new Error(error)
        }
    }
}